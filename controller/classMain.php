<?php
	require ("classDAO.php");
	require ("manejaAcceso.php");
	require ("manejaMenu.php");
	require ("manejaMuestra.php");
	require ("manejaEcs.php");
	require ("manejaEvaluar.php");
	require ("manejaUsuario.php");
	require ("manejaTipomuestra.php");
	
	class main // clase maestra
	{
		var $manejaAcceso;
		var $manejaMenu;
		var $codMenu;
		
		var $codcarMain;
		
		function estoyAqui()
		{
			echo 'estoy en la classMain.php';
		}
		
		function accesoLogin($boton, $user, $pwd)
		{
			$acceso=new manejaAcceso;
			for ($i=1; $i<3; $i++)
			{
				if ($i==1){$nombreUser=$acceso->loginUsuario($boton, $user, $pwd,$i);}
				if ($i==2){$codigoCargo=$acceso->cargoUsuario($boton, $user, $pwd,$i);}
			}
			
			$this->codcarMain=$codigoCargo;
			
			$descCargo=$acceso->descriptionCargo($codigoCargo);
			
			session_start();
			$_SESSION['xvar1'] = $nombreUser;
			$_SESSION['xvar2'] = $descCargo;
			$_SESSION['xvar3'] = $codigoCargo;
			
			$acceso->informacionUsuario();
		}
		
		function muestraMenu($codigo)
		{
			$tipoMenu=new manejaMenu;
			$tipoMenu->seleccionaMenu($codigo);
		}
		
		function manejaMuestrac($action, $value, $value2)
		{
			$analistaMuestra=new manejaMuestra;
			//$analistaMuestra->registraMuestrac($codigo,$descripc,$tipo);
			//$analistaMuestra->registraMuestram($codigo,$tipo, $nmuestra1, $descripm1, $nmuestra2, $descripm2, $nmuestra3, $descripm3);
			//$analistaMuestra->registraMuestrav($nmuestra1, $nvaso1, $nvaso2, $nvaso3, $nvaso4, $nvaso5, $nvaso6, $nvaso7, $nvaso8, $nvaso9, $nvaso10, $nvaso11, $nvaso12, $nmuestra2, $nvaso13, $nvaso14, $nvaso15, $nvaso16, $nvaso17, $nvaso18, $nvaso19, $nvaso20, $nvaso21, $nvaso22, $nvaso23, $nvaso24, $nmuestra3,$nvaso25, $nvaso26, $nvaso27, $nvaso28, $nvaso29, $nvaso30, $nvaso31, $nvaso32, $nvaso33, $nvaso34, $nvaso35, $nvaso36, $codigo);
			
			switch($action)
			{
				case 'General':
				$result = $analistaMuestra->registraMuestrac($value, $value2);
				return $result;
				break;
				
				case 'Detail':
				$result = $analistaMuestra->registraMuestram($value, $value2);
				return $result;
				break;
				
				case 'vasos':
				$analistaMuestra->registraMuestrav($value, $value2);
				}
			
		}
		
		function manejaMuestrab($codigo)
		{
			//cabecera
			$analistaMuestra=new manejaMuestra;
			$tipoMst=$analistaMuestra->consultaMuestrac($codigo);
			echo 'el coddigo de la muestra es: '.';'.$codigo;
			echo 'es tel el codigo de tipo de muestra en la classMain  '.$tipoMst;
			$descripTipomuestra=$analistaMuestra->descripcionTipomst($tipoMst);
			echo 'es la descripcion del tipo de muestra en la classMain  '.$descripTipomuestra;
			
			//descripcion de las muestras
			$descripMuestra1=$analistaMuestra->consultaMuestram($codigo,$tipoMst,'1');
			$descripMuestra2=$analistaMuestra->consultaMuestram($codigo,$tipoMst,'2');
			$descripMuestra3=$analistaMuestra->consultaMuestram($codigo,$tipoMst,'3');
			
			echo 'estas son las muestras '.$descripMuestra1.';'.$descripMuestra2.';'.$descripMuestra3; 
			
			//descripcion de los vasos
			$codVaso1=$analistaMuestra->consultaMuestrav($codigo,'1');
			$codVaso2=$analistaMuestra->consultaMuestrav($codigo,'2');
			$codVaso3=$analistaMuestra->consultaMuestrav($codigo,'3');
			$codVaso4=$analistaMuestra->consultaMuestrav($codigo,'4');
			$codVaso5=$analistaMuestra->consultaMuestrav($codigo,'5');
			$codVaso6=$analistaMuestra->consultaMuestrav($codigo,'6');
			$codVaso7=$analistaMuestra->consultaMuestrav($codigo,'7');
			$codVaso8=$analistaMuestra->consultaMuestrav($codigo,'8');
			$codVaso9=$analistaMuestra->consultaMuestrav($codigo,'9');
			$codVaso10=$analistaMuestra->consultaMuestrav($codigo,'10');
			$codVaso11=$analistaMuestra->consultaMuestrav($codigo,'11');
			$codVaso12=$analistaMuestra->consultaMuestrav($codigo,'12');
			
			$codVaso13=$analistaMuestra->consultaMuestrav($codigo,'13');
			$codVaso14=$analistaMuestra->consultaMuestrav($codigo,'14');
			$codVaso15=$analistaMuestra->consultaMuestrav($codigo,'15');
			$codVaso16=$analistaMuestra->consultaMuestrav($codigo,'16');
			$codVaso17=$analistaMuestra->consultaMuestrav($codigo,'17');
			$codVaso18=$analistaMuestra->consultaMuestrav($codigo,'18');
			$codVaso19=$analistaMuestra->consultaMuestrav($codigo,'19');
			$codVaso20=$analistaMuestra->consultaMuestrav($codigo,'20');
			$codVaso21=$analistaMuestra->consultaMuestrav($codigo,'21');
			$codVaso22=$analistaMuestra->consultaMuestrav($codigo,'22');
			$codVaso23=$analistaMuestra->consultaMuestrav($codigo,'23');
			$codVaso24=$analistaMuestra->consultaMuestrav($codigo,'24');
			
			$codVaso25=$analistaMuestra->consultaMuestrav($codigo,'25');
			$codVaso26=$analistaMuestra->consultaMuestrav($codigo,'26');
			$codVaso27=$analistaMuestra->consultaMuestrav($codigo,'27');
			$codVaso28=$analistaMuestra->consultaMuestrav($codigo,'28');
			$codVaso29=$analistaMuestra->consultaMuestrav($codigo,'29');
			$codVaso30=$analistaMuestra->consultaMuestrav($codigo,'30');
			$codVaso31=$analistaMuestra->consultaMuestrav($codigo,'31');
			$codVaso32=$analistaMuestra->consultaMuestrav($codigo,'32');
			$codVaso33=$analistaMuestra->consultaMuestrav($codigo,'33');
			$codVaso34=$analistaMuestra->consultaMuestrav($codigo,'34');
			$codVaso35=$analistaMuestra->consultaMuestrav($codigo,'35');
			$codVaso36=$analistaMuestra->consultaMuestrav($codigo,'36');
			
			//echo 'estos son los codigo de los vasos  '.$codVaso1.';'.$codVaso2.';'.$codVaso3.';'.$codVaso4.';'.$codVaso5.';'.$codVaso6.';'.$codVaso7.';'.$codVaso8.';'.$codVaso9.';'.$codVaso10.';'.$codVaso11.';'.$codVaso12.';'.$codVaso13.';'.$codVaso14.';'.$codVaso15.';'.$codVaso16.';'.$codVaso17.';'.$codVaso18.';'.$codVaso19.';'.$codVaso20.';'.$codVaso21.';'.$codVaso22.';'.$codVaso23.';'.$codVaso24.';'.$codVaso25.';'.$codVaso26.';'.$codVaso27.';'.$codVaso28.';'.$codVaso29.';'.$codVaso30.';'.$codVaso31.';'.$codVaso32.';'.$codVaso33.';'.$codVaso34.';'.$codVaso35.';'.$codVaso36; 
			session_start();
			
			//cabecera
			$_SESSION['tipoMuestra'] = $tipoMuestra;
			$_SESSION['descripTipomuestra'] = $descripTipomuestra;
			
			//descripcion de las muestras
			$_SESSION['descripMuestra1'] = $descripMuestra1;
			
			$_SESSION['descripMuestra2'] = $descripMuestra2;
			
			$_SESSION['descripMuestra3'] = $descripMuestra3;
			
			//descripcion de los vasos
			
			$_SESSION['codVaso1'] = $codVaso1;
			$_SESSION['codVaso2'] = $codVaso2;
			$_SESSION['codVaso3'] = $codVaso3;
			$_SESSION['codVaso4'] = $codVaso4;
			$_SESSION['codVaso5'] = $codVaso5;
			$_SESSION['codVaso6'] = $codVaso6;
			$_SESSION['codVaso7'] = $codVaso7;
			$_SESSION['codVaso8'] = $codVaso8;
			$_SESSION['codVaso9'] = $codVaso9;
			$_SESSION['codVaso10'] = $codVaso10;
			$_SESSION['codVaso11'] = $codVaso11;
			$_SESSION['codVaso12'] = $codVaso12;
			
			
			$_SESSION['codVaso13'] = $codVaso13;
			$_SESSION['codVaso14'] = $codVaso14;
			$_SESSION['codVaso15'] = $codVaso15;
			$_SESSION['codVaso16'] = $codVaso16;
			$_SESSION['codVaso17'] = $codVaso17;
			$_SESSION['codVaso18'] = $codVaso18;
			$_SESSION['codVaso19'] = $codVaso19;
			$_SESSION['codVaso20'] = $codVaso20;
			$_SESSION['codVaso21'] = $codVaso21;
			$_SESSION['codVaso22'] = $codVaso22;
			$_SESSION['codVaso23'] = $codVaso23;
			$_SESSION['codVaso24'] = $codVaso24;
			
			$_SESSION['codVaso25'] = $codVaso25;
			$_SESSION['codVaso26'] = $codVaso26;
			$_SESSION['codVaso27'] = $codVaso27;
			$_SESSION['codVaso28'] = $codVaso28;
			$_SESSION['codVaso29'] = $codVaso29;
			$_SESSION['codVaso30'] = $codVaso30;
			$_SESSION['codVaso31'] = $codVaso31;
			$_SESSION['codVaso32'] = $codVaso32;
			$_SESSION['codVaso33'] = $codVaso33;
			$_SESSION['codVaso34'] = $codVaso34;
			$_SESSION['codVaso35'] = $codVaso35;
			$_SESSION['codVaso36'] = $codVaso36;
		
			//$analistaMuestra->showInterface();
		}
		
		function manejaEcsinsert($codigo,$codjs,$descecs,$vaso1, $vaso2, $vaso3,$status)
		{
			echo $codigo.';'.$codjs.';'.$descecs.';'.$vaso1.';'.$vaso2.';'.$vaso3.';'.$status;
			//$analistaMstecs=new manejaEcs;
			//$regEcs=$analisMstecs->registroEcs($codigo,$codjs,$descecs, $vaso1, $vaso2, $vaso3, $status);
		}
		
		function manejeEcsselect($codigo)
		{
			$consultaMstecs=new manejaEcs;
			$descEcs=$consultaMstecs->consultaEcs($codigo,1);
			$nombrejs=$consultaMstecs->consultaEcs($codigo,2);
			$vaso1=$consultaMstecs->consultaEcs($codigo,3);
			$vaso2=$consultaMstecs->consultaEcs($codigo,4);
			$vaso3=$consultaMstecs->consultaEcs($codigo,5);
			
			echo 'datos de la consulta: '.  $descEcs.';'.$nombrejs.';'.$vaso1.';'.$vaso2.';'.$vaso3;
		}
		
		function manejeEcsupdate($codigo)
		{
			$consultaMstecs=new manejaEcs;
			$descEcs=$consultaMstecs->consultaEcs($codigo,1);
			$nombrejs=$consultaMstecs->consultaEcs($codigo,2);
			$vaso1=$consultaMstecs->consultaEcs($codigo,3);
			$vaso2=$consultaMstecs->consultaEcs($codigo,4);
			$vaso3=$consultaMstecs->consultaEcs($codigo,5);
			
			echo 'datos de la consulta: '.  $descEcs.';'.$nombrejs.';'.$vaso1.';'.$vaso2.';'.$vaso3;
		
			$actualizaMstecs=new manejaEcs;
			
			$actualiza=$actualizaMstecs->actualizaEcs1($codigo, $codjs);
		}
		
		function manejaEvaluarin($codiecs, $descrecs, $tipomstecs)
		{
			$instruccionEvaluar=new manejaEvaluar;
			$callInstruccion=$instruccionEvaluar->leerInstrucciones($codiecs, $descrecs, $tipomstecs);
		}			
		
		function manejaEvaluartipo($codiecs)
		{
			$evaluarEcs1=new manejaEvaluar;
			$callEvaluacion=$evaluarEcs1->evaluarEcs($codiecs);
		}
		
		function manejaEcsrecord($codEcs, $codTipomst, $codJuez, $fechaEcs, $horaEcs, $vasoEcs1, $vasoEcs2, $vasoEcs3, $cbx_est1, $cbx_lev1, $cbx_pan1, $cbx_dulce1, $cbx_cara1, $cbx_mfds1, $cbx_azu1, $cbx_luz1, $cbx_cuero1, $cbx_pacarton1, $cbx_acidoso1, $cbx_grc1, $cbx_rancio1, $cbx_metalico1, $cbx_vino1, $cbx_madera1, $cbx_otros1, $cbx_est2, $cbx_lev2, $cbx_pan2, $cbx_dulce2, $cbx_cara2, $cbx_mfds2, $cbx_azu2, $cbx_luz2, $cbx_cuero2, $cbx_pacarton2, $cbx_acidoso2, $cbx_grc2, $cbx_rancio2, $cbx_metalico2, $cbx_vino2, $cbx_madera2, $cbx_otros2, $cbx_est3, $cbx_lev3, $cbx_pan3, $cbx_dulce3, $cbx_cara3, $cbx_mfds3, $cbx_azu3, $cbx_luz3, $cbx_cuero3, $cbx_pacarton3, $cbx_acidoso3, $cbx_grc3, $cbx_rancio3, $cbx_metalico3, $cbx_vino3, $cbx_madera3, $cbx_otros3, $cbx_asp4, $cbx_pr4, $cbx_rem4, $cbx_ast4, $cbx_otr4, $cbx_asp5, $cbx_pr5, $cbx_rem5, $cbx_ast5, $cbx_otr5, $cbx_asp6, $cbx_pr6, $cbx_rem6, $cbx_ast6, $cbx_otr6, $resultEcs1, $resultEcs2, $resultEcs3, $resultEcs4, $resultEcs5, $resultEcs6, $resultEcs7, $resultEcs8, $resultEcs9)
		{
			echo $codEcs.';'.$codTipomst.';'.$codJuez.';'.$fechaEcs.';'.$horaEcs.';'.$vasoEcs1.';'.$vasoEcs2.';'.$vasoEcs3.';'.$cbx_est1.';'.$cbx_lev1.';'.$cbx_pan1.';'.$cbx_dulce1.';'.$cbx_cara1.';'.$cbx_mfds1.';'.$cbx_azu1.';'.$cbx_luz1.';'.$cbx_cuero1.';'.$cbx_pacarton1.';'.$cbx_acidoso1.';'.$cbx_grc1.';'.$cbx_rancio1.';'.$cbx_metalico1.';'.$cbx_vino1.';'.$cbx_madera1.';'.$cbx_otros1.';'.$cbx_est2.';'.$cbx_lev2.';'.$cbx_pan2.';'.$cbx_dulce2.';'.$cbx_cara2.';'.$cbx_mfds2.';'.$cbx_azu2.';'.$cbx_luz2.';'.$cbx_cuero2.';'.$cbx_pacarton2.';'.$cbx_acidoso2.';'.$cbx_grc2.';'.$cbx_rancio2.';'.$cbx_metalico2.';'.$cbx_vino2.';'.$cbx_madera2.';'.$cbx_otros2.';'.$cbx_est3.';'.$cbx_lev3.';'.$cbx_pan3.';'.$cbx_dulce3.';'.$cbx_cara3.';'.$cbx_mfds3.';'.$cbx_azu3.';'.$cbx_luz3.';'.$cbx_cuero3.';'.$cbx_pacarton3.';'.$cbx_acidoso3.';'. $cbx_grc3.';'. $cbx_rancio3.';'. $cbx_metalico3.';'. $cbx_vino3.';'. $cbx_madera3.';'. $cbx_otros3.';'. $cbx_asp4.';'. $cbx_pr4.';'. $cbx_rem4.';'. $cbx_ast4.';'. $cbx_otr4.';'. $cbx_asp5.';'. $cbx_pr5.';'. $cbx_rem5.';'. $cbx_ast5.';'. $cbx_otr5.';'. $cbx_asp6.';'. $cbx_pr6.';'. $cbx_rem6.';'. $cbx_ast6.';'. $cbx_otr6.';'. $resultEcs1.';'. $resultEcs2.';'. $resultEcs3.';'. $resultEcs4.';'. $resultEcs5.';'. $resultEcs6.';'. $resultEcs7.';'. $resultEcs8.';'. $resultEcs9;
			//$evaluarEcs28= new manejaEvaluar;
			//$evaluarC28ecs= $evaluarEcs28->recordEcs($codEcs, $codTipomst, $codJuez, $fechaEcs, $horaEcs, $vasoEcs1, $vasoEcs2, $vasoEcs3, $cbx_est1, $cbx_lev1, $cbx_pan1, $cbx_dulce1, $cbx_cara1, $cbx_mfds1, $cbx_azu1, $cbx_luz1, $cbx_cuero1, $cbx_pacarton1, $cbx_acidoso1, $cbx_grc1, $cbx_rancio1, $cbx_metalico1, $cbx_vino1, $cbx_madera1, $cbx_otros1, $cbx_est2, $cbx_lev2, $cbx_pan2, $cbx_dulce2, $cbx_cara2, $cbx_mfds2, $cbx_azu2, $cbx_luz2, $cbx_cuero2, $cbx_pacarton2, $cbx_acidoso2, $cbx_grc2, $cbx_rancio2, $cbx_metalico2, $cbx_vino2, $cbx_madera2, $cbx_otros2, $cbx_est3, $cbx_lev3, $cbx_pan3, $cbx_dulce3, $cbx_cara3, $cbx_mfds3, $cbx_azu3, $cbx_luz3, $cbx_cuero3, $cbx_pacarton3, $cbx_acidoso3, $cbx_grc3, $cbx_rancio3, $cbx_metalico3, $cbx_vino3, $cbx_madera3, $cbx_otros3, $cbx_asp4, $cbx_pr4, $cbx_rem4, $cbx_ast4, $cbx_otr4, $cbx_asp5, $cbx_pr5, $cbx_rem5, $cbx_ast5, $cbx_otr5, $cbx_asp6, $cbx_pr6, $cbx_rem6, $cbx_ast6, $cbx_otr6, $txt_lp1, $txt_lp2,$txt_lp3,$resultEcs1, $resultEcs2, $resultEcs3, $resultEcs4, $resultEcs5, $resultEcs6, $resultEcs7, $resultEcs8, $resultEcs9);
		}
		
		function manejaUsuarioRec($tx_username, $tx_pwd,$cbx_tipoUsuario, $tx_pnuser, $tx_snuser, $tx_pauser, $tx_sauser, $tx_ciuser, $tx_dir1user, $tx_dir2user, $tx_tlf1user, $tx_tlf2user, $tx_tlf3user, $tx_emailuser)
		{
			$insManejousuario=new manejaUsuario;
			$recordUser=$insManejousuario->registraUsuario($tx_username, $tx_pwd,$cbx_tipoUsuario, $tx_pnuser, $tx_snuser, $tx_pauser, $tx_sauser, $tx_ciuser, $tx_dir1user, $tx_dir2user, $tx_tlf1user, $tx_tlf2user, $tx_tlf3user, $tx_emailuser);
		}
		
		function manejaUsuarioupt($tx_username, $cbx_tipoUsuario, $tx_pnuser, $tx_snuser, $tx_pauser, $tx_sauser, $tx_dir1user, $tx_dir2user, $tx_tlf1user, $tx_tlf2user, $tx_tlf3user, $tx_emailuser)
		{
			$insManejousuario=new manejaUsuario;
			$updateUser=$insManejousuario->actualizaUsuario($tx_username, $cbx_tipoUsuario, $tx_pnuser, $tx_snuser, $tx_pauser, $tx_sauser, $tx_dir1user, $tx_dir2user, $tx_tlf1user, $tx_tlf2user, $tx_tlf3user, $tx_emailuser);
		}
		
		function manejaUsuariostatus($cbx_appUsuario, $cbx_statusUsuario)
		{
			$insManejousuario=new manejaUsuario;
			$statusUser=$insManejousuario->aodUsuario($cbx_appUsuario, $cbx_statusUsuario);
		}
		
		function manejaUsuarioresetpwd($cbx_appUsuario, $pwdUser)
		{
			$insManejousuario=new manejaUsuario;
			$resetUser=$insManejousuario->reseteaUsuario($cbx_appUsuario, $pwdUser);
		}
		
		function manejaTipomuestrarecord($codigo, $descTipomst, $instruc)
		{
			$insManejotipomst= new manejaTipomuestra;
			$muestraTipo=$insManejotipomst->registraTipomuestra($codigo, $descTipomst, $instruc);
			echo 'esto es lo que regresa '.';'.$muestraTipo;
		}
		
		function manejaTipomuestraupdate($codigo, $descTipomst, $instruc)
		{
			$insManejotipomst= new manejaTipomuestra;
			$updatemuestraTipo=$insManejotipomst->actualizaTipomuestra($codigo, $descTipomst, $instruc);
			echo 'esto es lo que regresa '.';'.$muestraTipo;
		}
		
		function manejaTipomuestraaod($codigo, $statusMst)
		{
			$insManejotipomst= new manejaTipomuestra;
			$aodmuestraTipo=$insManejotipomst->aodTipomuestra($codigo, $statusMst);
		}
		
		function errorPantalla()
		{
			echo 'USTED A DECIDIDO PRECIONAR EL BOTON DE CANCELAR';
		}
	}
	$clase= new main;
	
	if (isset($_POST['BT_ACEPTAR'])) {$clase->accesoLogin("BT_ACEPTAR",$_POST['txt_user'], $_POST['txt_pwd']);} //llama a la funcion del boton aceptar index.php
	if (isset($_POST['BT_CANCELAR'])) {$clase->accesoLogin("BT_CANCELAR",$_POST['txt_user'], $_POST['txt_pwd']);} //llama a la funcion del boton cancelar index.php
	
	if (isset($_POST['BT_ACPTRUINFO'])) {$clase->muestraMenu($_POST['codigoCargo']);} //llama a la funcion del boton aceptar de infoUsuario.php
	if (isset($_POST['BT_CNCLRUINFO'])) {$clase->muestraMenu($_POST['codigoCargo']);} //llama a la funcion del boton cancelar de infoUsuario.php
	
	if (isset($_POST['bt_acregmuest'])) {
		
		$MuGe = $_POST['tx_descgral'];
		$type = $_POST['cbx_tipomuestra'];
		
		$ResultGeneral = $clase->manejaMuestrac('General', $MuGe, $type);
		
		for($m=1;$m<4;$m++)
		{
			$MuDe = $_POST['tx_desmst_'.$m];
			
				
			$ResultDetail = $clase->manejaMuestrac('Detail', $MuDe, $ResultGeneral);
			
			for($v=1;$v<13;$v++)
			{
			$vaso = $_POST['tx_m'.$m.'_v'.$v];
			
			$clase->manejaMuestrac('vasos', $vaso, $ResultDetail);
			echo $vaso.$resultDetail.$ResultGeneral;
			}
		
			
		}
		
		//$clase->manejaMuestrac($_POST['tx_codgral'], $_POST['tx_descgral'],$_POST['cbx_tipomuestra'],$_POST['tx_muestra_1'],$_POST['tx_desmst_1'],$_POST['tx_file11'],$_POST['tx_file12'],$_POST['tx_file13'],$_POST['tx_file14'],$_POST['tx_file15'],$_POST['tx_file16'],$_POST['tx_file17'],$_POST['tx_file18'],$_POST['tx_file19'],$_POST['tx_file110'],$_POST['tx_file111'],$_POST['tx_file112'],$_POST['tx_muestra_2'],$_POST['tx_desmst_2'],$_POST['tx_file21'],$_POST['tx_file22'],$_POST['tx_file23'],$_POST['tx_file24'],$_POST['tx_file25'],$_POST['tx_file26'],$_POST['tx_file27'],$_POST['tx_file28'],$_POST['tx_file29'],$_POST['tx_file210'],$_POST['tx_file211'],$_POST['tx_file212'],$_POST['tx_muestra_3'],$_POST['tx_desmst_3'],$_POST['tx_file31'],$_POST['tx_file32'],$_POST['tx_file33'],$_POST['tx_file34'],$_POST['tx_file35'],$_POST['tx_file36'],$_POST['tx_file37'],$_POST['tx_file38'],$_POST['tx_file39'],$_POST['tx_file310'],$_POST['tx_file311'],$_POST['tx_file312']);
		} //llama a la funcion del boton aceptar de registraMuestras.php
	if (isset($_POST['bt_caregmuest'])) {$clase->errorPantalla();} //llama a la funcion del boton cancelar de registraMuestras.php
//
	if (isset($_POST['bt_buscamuestra'])) {$clase->manejaMuestrab($_POST['cbx_cargamuestra']);} //llama a la funcion del boton aceptar de consultaMuestra.php
	
	if (isset($_POST['btn_aregecs'])) {$clase->manejaEcsinsert($_POST['cbx_descgral'],$_POST['cbx_juezsenso'],$_POST['tx_regecs'],$_POST['cbx_vaso1'],$_POST['cbx_vaso2'],$_POST['cbx_vaso3'], "0");} //llama a la funcion del boton aceptar de registraEcs.php
	if (isset($_POST['btn_cregecs'])) {$clase->errorPantalla();} //llama a la funcion del boton cancelar de registraEcs.php
	
	if (isset($_POST['btn_aconsecs'])) {$clase->manejeEcsselect($_POST['cbx_evaecs']);} //llama a la funcion del boton aceptar de consultaMuestra.php
	if (isset($_POST['btn_aconsecs'])) {$clase->errorPantalla();} //llama a la funcion del boton cancelar de consultaMuestra.php
	
	if (isset($_POST['btn_aactecs'])) {$clase->manejeEcsupdate($_POST['cbx_aevaecs'], $_POST['cbx_juezsenso1']);} //llama a la funcion del boton aceptar de actualizarEcs.php
	if (isset($_POST['btn_cactecs'])) {$clase->errorPantalla();} //llama a la funcion del boton cancelar de actualizarEcs.php
	
	if (isset($_POST['btn_alistaecs'])) {$clase->manejaEcslist($_POST['cbx_listaecs']);} //llama a la funcion del boton aceptar de listaEcs.php
	if (isset($_POST['btn_clistaecs'])) {$clase->errorPantalla();} //llama a la funcion del boton cancelar de listaEcs.php
	
	if (isset($_POST['btn_opt1'])) {$clase->manejaEvaluarin($_POST['cg1'], $_POST['dc1'],$_POST['tipmst1']);} //llama a la funcion del boton aceptar de evaluarEcs.php
	if (isset($_POST['btn_opt2'])) {$clase->manejaEvaluarin($_POST['cg2'], $_POST['dc2'],$_POST['tipmst2']);} //llama a la funcion del boton aceptar de evaluarEcs.php
	if (isset($_POST['btn_opt3'])) {$clase->manejaEvaluarin($_POST['cg3'], $_POST['dc3'],$_POST['tipmst3']);} //llama a la funcion del boton aceptar de evaluarEcs.php
	if (isset($_POST['btn_opt4'])) {$clase->manejaEvaluarin($_POST['cg4'], $_POST['dc4'],$_POST['tipmst4']);} //llama a la funcion del boton aceptar de evaluarEcs.php
	if (isset($_POST['btn_opt5'])) {$clase->manejaEvaluarin($_POST['cg5'], $_POST['dc5'],$_POST['tipmst5']);} //llama a la funcion del boton aceptar de evaluarEcs.php
	if (isset($_POST['btn_opt6'])) {$clase->manejaEvaluarin($_POST['cg6'], $_POST['dc6'],$_POST['tipmst6']);} //llama a la funcion del boton aceptar de evaluarEcs.php
	if (isset($_POST['btn_opt7'])) {$clase->manejaEvaluarin($_POST['cg7'], $_POST['dc7'],$_POST['tipmst7']);} //llama a la funcion del boton aceptar de evaluarEcs.php
	if (isset($_POST['btn_opt8'])) {$clase->manejaEvaluarin($_POST['cg8'], $_POST['dc8'],$_POST['tipmst8']);} //llama a la funcion del boton aceptar de evaluarEcs.php
	if (isset($_POST['btn_opt9'])) {$clase->manejaEvaluarin($_POST['cg9'], $_POST['dc9'],$_POST['tipmst9']);} //llama a la funcion del boton aceptar de evaluarEcs.php
	
	if (isset($_POST['btn_ainstrud'])) {$clase->manejaEvaluartipo($_POST['codEcs']);} //llama a la funcion del boton aceptar de instruccionEcs.php
	
	if (isset($_POST['btn_aecscer'])) {$clase->manejaEcsrecord($_POST['codEcs'], $_POST['tipoMst'],$_POST['$fechaEcs'], $_POST['$horaEcs'], $_POST['vasoEcs1'], $_POST['vasoEcs2'], $_POST['vasoEcs3'], $_POST['cbx_est1'], $_POST['cbx_lev1'], $_POST['cbx_pan1'], $_POST['cbx_dulce1'], $_POST['cbx_cara1'], $_POST['cbx_mfds1'], $_POST['cbx_azu1'], $_POST['cbx_luz1'], $_POST['cbx_cuero1'], $_POST['cbx_pacarton1'], $_POST['cbx_acidoso1'], $_POST['cbx_grc1'], $_POST['cbx_rancio1'], $_POST['cbx_metalico1'], $_POST['cbx_vino1'], $_POST['cbx_madera1'], $_POST['cbx_otros1'], $_POST['cbx_est2'], $_POST['cbx_lev2'], $_POST['cbx_pan2'], $_POST['cbx_dulce2'], $_POST['cbx_cara2'], $_POST['cbx_mfds2'], $_POST['cbx_azu2'], $_POST['cbx_luz2'], $_POST['cbx_cuero2'], $_POST['cbx_pacarton2'], $_POST['cbx_acidoso2'], $_POST['cbx_grc2'], $_POST['cbx_rancio2'], $_POST['cbx_metalico2'], $_POST['cbx_vino2'], $_POST['cbx_madera2'], $_POST['cbx_otros2'], $_POST['cbx_est3'], $_POST['cbx_lev3'], $_POST['cbx_pan3'], $_POST['cbx_dulce3'], $_POST['cbx_cara3'], $_POST['cbx_mfds3'], $_POST['cbx_azu3'], $_POST['cbx_luz3'], $_POST['cbx_cuero3'], $_POST['cbx_pacarton3'], $_POST['cbx_acidoso3'], $_POST['cbx_grc3'], $_POST['cbx_rancio3'], $_POST['cbx_metalico3'], $_POST['cbx_vino3'], $_POST['cbx_madera3'], $_POST['cbx_otros3'], $_POST['cbx_asp4'], $_POST['cbx_pr4'], $_POST['cbx_rem4'], $_POST['cbx_ast4'], $_POST['cbx_otr4'], $_POST['cbx_asp5'], $_POST['cbx_pr5'], $_POST['cbx_rem5'], $_POST['cbx_ast5'], $_POST['cbx_otr5'], $_POST['cbx_asp6'], $_POST['cbx_pr6'], $_POST['cbx_rem6'], $_POST['cbx_ast6'], $_POST['cbx_otr6'], $_POST['txt_lp1'],$_POST['txt_lp2'],$_POST['txt_lp3'],$_POST['resultEcs1'], $_POST['resultEcs2'], $_POST['resultEcs3'], $_POST['resultEcs4'], $_POST['resultEcs5'], $_POST['resultEcs6'], $_POST['resultEcs7'], $_POST['resultEcs8'], $_POST['resultEcs9']);} //llama a la funcion del boton aceptar de ecsCer28.php
	if (isset($_POST['btn_cecscer'])) {$clase->errorPantalla();} //llama a la funcion del boton cancelar de ecsCer28.php
	
	if (isset($_POST['btna_ruserfrm'])) {$clase->manejaUsuarioRec($_POST['tx_username'], $_POST['tx_pwd'],$_POST['cbx_tipoUsuario'], $_POST['tx_pnuser'], $_POST['tx_snuser'], $_POST['tx_pauser'], $_POST['tx_sauser'], $_POST['tx_ciuser'], $_POST['tx_dir1user'], $_POST['tx_dir2user'], $_POST['tx_tlf1user'], $_POST['tx_tlf2user'], $_POST['tx_tlf3user'], $_POST['tx_emailuser']);} //llama a la funcion del boton aceptar de registraUser.php
	if (isset($_POST['btnc_ruserfrm'])) {$clase->errorPantalla();} //llama a la funcion del boton cancelar de registraUser.php
	
	if (isset($_POST['btna_actuserfrm'])) {$clase->manejaUsuarioupt($_POST['cbx_appUsuario'], $_POST['cbx_tipoUsuario'], $_POST['tx_pnuser'], $_POST['tx_snuser'], $_POST['tx_pauser'], $_POST['tx_sauser'], $_POST['tx_dir1user'], $_POST['tx_dir2user'], $_POST['tx_tlf1user'], $_POST['tx_tlf2user'], $_POST['tx_tlf3user'], $_POST['tx_emailuser']);} //llama a la funcion del boton aceptar de actualizaUser.php
	if (isset($_POST['btnc_actuserfrm'])) {$clase->errorPantalla();} //llama a la funcion del boton cancelar de actualizaUser.php
		
	if (isset($_POST['btna_aoduserfrm'])) {$clase->manejaUsuariostatus($_POST['cbx_appUsuario'], $_POST['cbx_statusUsuario']);} //llama a la funcion del boton aceptar de adUser.php
	if (isset($_POST['btnc_aoduserfrm'])) {$clase->errorPantalla();} //llama a la funcion del boton cancelar de adUser.php
	
	if (isset($_POST['btna_rpuserfrm'])) {$clase->manejaUsuarioresetpwd($_POST['cbx_appUsuario'], $_POST['tx_clave']);} //llama a la funcion del boton aceptar de adUser.php
	if (isset($_POST['btnc_rpuserfrm'])) {$clase->errorPantalla();} //llama a la funcion del boton cancelar de adUser.php
	
	if (isset($_POST['btna_restmst'])) {$clase->manejaTipomuestrarecord($_POST['codMst'], $_POST['tx_descTipomst'], $_POST['tx_descInstr']);} //llama a la funcion del boton aceptar de adUser.php
	if (isset($_POST['btnc_restmst'])) {$clase->errorPantalla();} //llama a la funcion del boton cancelar de adUser.php
	
	if (isset($_POST['btna_changeinst'])) {$clase->manejaTipomuestraupdate($_POST['cbx_tipomst'], $_POST['tx_changedesc'], $_POST['tx_changeinst']);} //llama a la funcion del boton aceptar de adUser.php
	if (isset($_POST['btnc_changeinst'])) {$clase->errorPantalla();} //llama a la funcion del boton cancelar de adUser.php
	
	if (isset($_POST['btna_aodmst'])) {$clase-> manejaTipomuestraaod($_POST['cbx_tipomst'], $_POST['cbx_statusMst']);} //llama a la funcion del boton aceptar de adUser.php
	if (isset($_POST['btnc_aodmst'])) {$clase->errorPantalla();} //llama a la funcion del boton cancelar de adUser.php
	
	//$clase->estoyAqui();
?>
