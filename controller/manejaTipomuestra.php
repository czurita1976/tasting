<?php
	class manejaTipomuestra
	{
		function registraTipomuestra($codigo, $descTipomst, $instruc)
		{
			$conndb=new DAOsql;
			$datosTipomuestra=$conndb->insertTipomuestras($codigo, $descTipomst, $instruc);
		}
		
		function actualizaTipomuestra($codigo, $descTipomst, $instruc)
		{
			$conndb=new DAOsql;
			$datosTipomuestra1=$conndb->updateTipomuestras($codigo, $descTipomst, $instruc);
		}
		
		function aodTipomuestra($codigo, $statusMst)
		{
			$conndb=new DAOsql;
			$datosTipomuestra2=$conndb->updateTipomuestrasaod($codigo, $statusMst);
		}
				
		function informacionUsuario()
		{
			echo ("<script  language='javascript'>
					self.location ='../ui/userInformation.php';
				</script>");
		}
	}
?>
