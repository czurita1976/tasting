<?php
	class manejaEvaluar
	{
		function leerInstrucciones($codiecs, $descrecs, $tipomstecs)
		{
			$conndb=new DAOsql;
			$nombreTipomst=$conndb->finddescripTipomst($tipomstecs);
			$varInstruccion=$conndb->findInstrucciones($tipomstecs);
			
			session_start();
			$_SESSION['codEcs'] = $codiecs;
			$_SESSION['descripTipomuestra'] = $descrecs;
			$_SESSION['tipoMuestra1'] = $nombreTipomst;
			$_SESSION['varInstrucciones'] = $varInstruccion;

			
			echo ("<script  language='javascript'>
					self.location ='../ui/instruccionEcs.php';
				</script>");
		}
		
		function evaluarEcs($codigo)
		{
			$conndb=new DAOsql;
			$infoEcs=$conndb->findEcsevaluar($codigo);
			//var_dump($infoEcs);
			session_start();
			$contar=0;
			foreach ($infoEcs as $row)
			{ 
				if ($contar==0 && $row['tm_codgral']!=0) 
				{
					$_SESSION['$cg1']=$row['tm_codgral'];
					$_SESSION['$dc1']=$row['tm_descrip'];
					$_SESSION['$tipmst1']=$row['tm_tipmst'];
					$_SESSION['$codJuse']=$row['tm_codjs'];
					$_SESSION['$vasoEcs1']=$row['tm_codv1'];
					$_SESSION['$vasoEcs2']=$row['tm_codv2'];
					$_SESSION['$vasoEcs3']=$row['tm_codv3'];
					$_SESSION['$nombreTipomst1']=$conndb->finddescripTipomst($row['tm_tipmst']);
				}
				$contar++;
			}
			unset($row);
			
			echo ("<script  language='javascript'>
					self.location ='../ui/ECS/ecsCer28.php';
				</script>");
		}
		
		function recordEcs($codEcs, $codTipomst, $codJuez, $fechaEcs, $horaEcs, $vasoEcs1, $vasoEcs2, $vasoEcs3, $cbx_est1, $cbx_lev1, $cbx_pan1, $cbx_dulce1, $cbx_cara1, $cbx_mfds1, $cbx_azu1, $cbx_luz1, $cbx_cuero1, $cbx_pacarton1, $cbx_acidoso1, $cbx_grc1, $cbx_rancio1, $cbx_metalico1, $cbx_vino1, $cbx_madera1, $cbx_otros1, $cbx_est2, $cbx_lev2, $cbx_pan2, $cbx_dulce2, $cbx_cara2, $cbx_mfds2, $cbx_azu2, $cbx_luz2, $cbx_cuero2, $cbx_pacarton2, $cbx_acidoso2, $cbx_grc2, $cbx_rancio2, $cbx_metalico2, $cbx_vino2, $cbx_madera2, $cbx_otros2, $cbx_est3, $cbx_lev3, $cbx_pan3, $cbx_dulce3, $cbx_cara3, $cbx_mfds3, $cbx_azu3, $cbx_luz3, $cbx_cuero3, $cbx_pacarton3, $cbx_acidoso3, $cbx_grc3, $cbx_rancio3, $cbx_metalico3, $cbx_vino3, $cbx_madera3, $cbx_otros3, $cbx_asp4, $cbx_pr4, $cbx_rem4, $cbx_ast4, $cbx_otr4, $cbx_asp5, $cbx_pr5, $cbx_rem5, $cbx_ast5, $cbx_otr5, $cbx_asp6, $cbx_pr6, $cbx_rem6, $cbx_ast6, $cbx_otr6, $txt_lp1, $txt_lp2,$txt_lp3,$resultEcs1, $resultEcs2, $resultEcs3, $resultEcs4, $resultEcs5, $resultEcs6, $resultEcs7, $resultEcs8, $resultEcs9)
		{
			//instancio la clase DAOsql
			$conndb=new DAOsql;
			
			//script para el insert de la cabecera para las evaluaciones de cerveza 28 
			$evaluarC28=$conndb->recordEvaluarc28($codEcs, $codTipomst, $codJuez, $fechaEcs, $horaEcs);
			  
			//script para el insert de los detalles para las evaluaciones de cerveza 28 para el vaso # 1
			$evaluarD128=$conndb->recordEvaluard28($codEcs, $vasoEcs1, $cbx_est1, $cbx_lev1, $cbx_pan1, $cbx_dulce1, $cbx_cara1, $cbx_mfds1, $cbx_azu1, $cbx_luz1, $cbx_cuero1, $cbx_pacarton1, $cbx_acidoso1, $cbx_grc1, $cbx_rancio1, $cbx_metalico1, $cbx_vino1, $cbx_madera1, $cbx_otros1, $resultEcs1, $cbx_asp4, $cbx_pr4, $cbx_rem4, $cbx_ast4, $cbx_otr4, $resultEcs4, $resultEcs7, $txt_lp1);
			
			//script para el insert de los detalles para las evaluaciones de cerveza 28 para el vaso # 2
			$evaluarD228=$conndb->recordEvaluard28($codEcs, $vasoEcs2, $cbx_est2, $cbx_lev2, $cbx_pan2, $cbx_dulce2, $cbx_cara2, $cbx_mfds2, $cbx_azu2, $cbx_luz2, $cbx_cuero2, $cbx_pacarton2, $cbx_acidoso2, $cbx_grc2, $cbx_rancio2, $cbx_metalico2, $cbx_vino2, $cbx_madera2, $cbx_otros2, $resultEcs2, $cbx_asp5, $cbx_pr5, $cbx_rem5, $cbx_ast5, $cbx_otr5, $resultEcs5, $resultEcs8, $txt_lp2);
			
			//script para el insert de los detalles para las evaluaciones de cerveza 28 para el vaso # 3
			$evaluarD328=$conndb->recordEvaluard28($codEcs, $vasoEcs3, $cbx_est3, $cbx_lev3, $cbx_pan3, $cbx_dulce3, $cbx_cara3, $cbx_mfds3, $cbx_azu3, $cbx_luz3, $cbx_cuero3, $cbx_pacarton3, $cbx_acidoso3, $cbx_grc3, $cbx_rancio3, $cbx_metalico3, $cbx_vino3, $cbx_madera3, $cbx_otros3, $resultEcs3, $cbx_asp6, $cbx_pr6, $cbx_rem6, $cbx_ast6, $cbx_otr6, $resultEcs6, $resultEcs9, $txt_lp3);
			
			//script para el cambio del estatus de la evaluacion de calidad sensorial
			$actualizaEstatus=$conn->updateStatustest($codEcs);
		
			//devolver al menu principal
		}
				
		function informacionUsuario()
		{
			
		}
	}
?>
