<?php
	require("classConexionbd.php");
	
	class DAOsql
	{
		var $conn;
		var $rs;
	
		public function __construct() {$this->conn=new conectarDb();}
	
		//VALIDAR LA IDENTIDAD DEL USUARIO en el maestro de usuarios
		function accesoLogin($usuario, $clave, $dato)
		{
			//sql para VALIDAR LA IDENTIDAD DEL USUARIO en el maestro de usuarios
			$sql="SELECT iduser_usrm, nuser_usrm, pwduser_usrm, pnuser_usrm, snuser_usrm, 
			pauser_usrm, sauser_usrm, ciuser_usrm, codcar_usrm, dir1user_usrm, 
			dir2user_usrm, tlf1user_usrm, tlf2user_usrm, tlf3user_usrm, emailuser_usrm
			FROM taste_user_m where nuser_usrm='$usuario' AND pwduser_usrm='$clave'";
        
	
			$this->rs=pg_query($sql) or die ('ERROR AL HACER EL SELECT DEL USUARIO Y PASSWORD' . pg_result_error());
			if ($this->rs)
			{
				$resulsql = @pg_fetch_array($this->rs,0);
				
				if ($resulsql[1]=$usuario && $resulsql[2]=$clave)
				{
					if ($dato='1') // condicional para devolver el nombre del usuario
					{
						$nombreUsuario = $resulsql[3].' '.$resulsql[4].' '.$resulsql[5].' '.$resulsql[6];
						return $nombreUsuario;		
					}
					if ($dato='2') // condicional para devolver el codigo del cargo del usuario
					{
						$codCargo = $resulsql[9];
						return $codCargo;		
					}
				}
				else
				{
					return "EL usuario o clave no son los correctos".":".$resulsql[1].":".$usuario.":".$clave;
				}
			}
			else
			{
				echo 'no se conecto';
			}	
			$resulsql = pg_fetch_all($this->rs);
			print_r ($resulsql);
		}
		
		//extrae el codigo de cargo del usuario de la aplicacion movil 
		function cargoUser($usuario, $clave, $dato)
		{
			//sql para extraer la informacion del usuario en el maestro de usuarios
			$sql="SELECT iduser_usrm, nuser_usrm, pwduser_usrm, pnuser_usrm, snuser_usrm, 
			pauser_usrm, sauser_usrm, ciuser_usrm, codcar_usrm, dir1user_usrm, 
			dir2user_usrm, tlf1user_usrm, tlf2user_usrm, tlf3user_usrm, emailuser_usrm
			FROM taste_user_m where nuser_usrm='$usuario' AND pwduser_usrm='$clave'";
        
	
			$this->rs=pg_query($sql) or die ('ERROR AL HACER EL SELECT DEL USUARIO Y PASSWORD' . pg_result_error());
			if ($this->rs)
			{
				$resulsql = @pg_fetch_array($this->rs,0);
				
				if ($resulsql[1]=$usuario && $resulsql[2]=$clave)
				{
					if ($dato='2') // condicional para devolver el codigo del cargo del usuario
					{
						$codCargo = $resulsql[8];
						return $codCargo;		
					}
				}
				else
				{
					return "EL usuario o clave no son los correctos".":".$resulsql[1].":".$usuario.":".$clave;
				}
			}
			else
			{
				echo 'no se conecto';
			}	
			$resulsql = pg_fetch_all($this->rs);
			print_r ($resulsql);
		}
		
		//extrae la descripcion del cargo del usuario de la aplicacion movil 
		function descriptionUser($idcargo)
		{
			//sql para extraer la informacion de la descripcion del cargo del maestro de cargos 
			$sql="SELECT idcargo_cgum, descrip_cgum FROM taste_cargos_m where idcargo_cgum='$idcargo'";
        
	
			$this->rs=pg_query($sql) or die ('ERROR AL HACER EL SELECT DE LA DESCRIPCION DEL CARGO' . pg_result_error());
			if ($this->rs)
			{
				$resulsql = @pg_fetch_array($this->rs,0);
				$descripCargo = $resulsql[1];
				return $descripCargo;		
			}
			else
			{
				echo 'no se conecto';
			}	
			$resulsql = pg_fetch_all($this->rs);
			print_r ($resulsql);
		}
		
		//hace el insert en la base de datos taste_mst_cargac
		function recordMuestrac($descripc,$tipo)
		{
			//sql para hacer el insert en la BD taste_mst_cargac 
			$sql="INSERT INTO taste_mst_cargac(desc_carga_mst, tpev_carga_mst) VALUES ('$descripc','$tipo')";
        
			$this->rs=pg_query($sql) or die ('ERROR AL HACER EL INSERT DE LA CARGA DE MUESTRA CABECERA'.$descripc.';'.$tipo. pg_result_error());
			
			$sql2 = "SELECT id_carga_mst from taste_mst_cargac order by id_carga_mst DESC Limit 1";
			
			$this->rs=pg_query($sql2) or die ('ERROR AL HACER EL INSERT DE LA CARGA DE MUESTRA' . pg_result_error());
			if ($this->rs)
			{
				$resulsql = @pg_fetch_array($this->rs,0);
				return $resulsql[0];
			}
			else
			{
				echo 'no se conecto';
			}	
			$resulsql = pg_fetch_all($this->rs);
			print_r ($resulsql);
		}
		
		//hace el insert en la base de datos taste_mst_cargam	
		function recordMuestram($descmst, $id_mstgral)
		{
			$sql="INSERT INTO public.taste_mst_cargam(mst_idcargral, mst_cantvs, mst_decpmst) VALUES ('$id_mstgral', 12, '$descmst')";
			
			$this->rs=pg_query($sql) or die ('ERROR AL HACER EL INSERT EN LA CARGA DE LAS MUESTRAS DETALLE' . pg_result_error());
			
			$sql2 = "SELECT mst_idmst from taste_mst_cargam order by mst_idmst DESC Limit 1";
			
			$this->rs=pg_query($sql2) or die ('ERROR AL HACER EL INSERT EN LA CARGA DE LAS MUESTRAS' . pg_result_error());
							
			if ($this->rs)
			{
				$resulsql = @pg_fetch_array($this->rs,0);
				return $resulsql[0];
			}
			else
			{
				echo 'no se conecto';
			}	
			$resulsql = pg_fetch_all($this->rs);
			print_r ($resulsql);
		}
		
		//hace el insert en la base de datos taste_mst_cargav
		function recordMuestrav($nvaso, $nmuestra) 
		{
			$sql="INSERT INTO public.taste_mst_cargav(mstcv_nummst, mstcv_nvaso, mstcv_status) VALUES ('$nmuestra', '$nvaso', '0')";
			
			$this->rs=pg_query($sql) or die ('ERROR AL HACER EL INSERT DE LAS CARGA DE VASOS' . pg_result_error());
			
			if ($this->rs){$resulsqlv=1;}
			else {$resulsqlv=0;}
				
			$resulsqlv = pg_fetch_all($this->rs);
			
			
			return $resulsqlv;
		}
		
		//hace el select en la base de datos taste_mst_cargac
		function findMuestrac($codigo)
		{
			$sql="SELECT id_carga_mst, desc_carga_mst, tpev_carga_mst FROM public.taste_mst_cargac WHERE (id_carga_mst='$codigo')";
        
			$this->rs=pg_query($sql) or die ('ERROR AL HACER EL SELECT taste_mst_cargac' . pg_result_error());
			if ($this->rs)
			{
				$resulsql = @pg_fetch_array($this->rs,0);
				return $resulsql[2];
			}
			else
			{
				echo 'no se conecto';
			}	
			$resulsql = pg_fetch_all($this->rs);
			print_r ($resulsql);
		}
		
		//sql para extraer la idescripcion de tipo de muestra general 
		function finddescripTipomst($codigo)
		{
			$sql="SELECT idevalscm_ttm, descecscm_ttm FROM taste_tasting_m WHERE idevalscm_ttm='$codigo'"; 

			$this->rs=pg_query($sql) or die ('ERROR AL HACER EL SELECT de tipo de muestra general ' . pg_result_error());
			if ($this->rs)
			{
				$resulsql = @pg_fetch_array($this->rs,0);
				$descripTipomst = $resulsql[1];
				return $descripTipomst;		
			}
			else
			{
				echo 'no se conecto';
			}	
			$resulsql = pg_fetch_all($this->rs);
			print_r ($resulsql);
		}
		
		//hace el select en la base de datos taste_tasting_m
		function findMuestram($codigo, $tipo)
		{
			$sql="SELECT mst_idcargral, mst_tipo, mst_nummst, mst_decpmst  FROM public.taste_mst_cargam WHERE mst_idcargral='$codigo' AND mst_tipo='$tipo'";
        
			$this->rs=pg_query($sql) or die ('ERROR AL HACER EL SELECT DE datos taste_tasting_m' . pg_result_error());
			for($i = 1; $arreglo[$i] = @pg_fetch_array($this->rs); $i++);
			return $arreglo;
		}
	
		//hace el select en la base de datos taste_mst_cargav
		function findMuestrav($codigo)
		{
			$sql="SELECT mstcv_id, mstcv_codgral, mstcv_nummst, mstcv_nvaso, mstcv_status FROM public.taste_mst_cargav WHERE mstcv_codgral='$codigo' ORDER BY mstcv_id ASC";
        
			$this->rs=pg_query($sql) or die ('ERROR AL HACER EL SELECT DE datos taste_mst_cargav' . pg_result_error());
			for($i = 1; $arreglo[$i] = @pg_fetch_array($this->rs); $i++);
			return $arreglo;
		}
		
		//hace el insert en la base de datos taste_testmain
		function recordTestmain($codigo,$tipo,$codjs, $vaso1, $vaso2, $vaso3, $status,$descecs)
		{
			$sql="INSERT INTO public.taste_testmain(tm_codgral, tm_tipmst, tm_codjs, tm_codv1, tm_codv2, tm_codv3, tm_status, tm_descrip) VALUES ('$codigo','$tipo','$codjs', '$vaso1', '$vaso2', '$vaso3', '$status','$descecs')";
			$this->rs=pg_query($sql) or die ('ERROR AL HACER EL INSERT de datos taste_testmain' . pg_result_error());
			if ($this->rs){$resulsqlm=1;}
			else{$resulsqlm=0;}
			return $resulsqlm;
		}
		
		//hace el select en la base de datos taste_testmain
		function findConsultaecs($codigo, $prmt)
		{
			$sql="SELECT tm_codgral, tm_tipmst, tm_codjs, tm_codv1, tm_codv2, tm_codv3, tm_status, tm_descrip FROM public.taste_testmain WHERE tm_codgral='$codigo' ORDER BY tm_descrip ASC";
        
			$this->rs=pg_query($sql) or die ('ERROR AL HACER EL SELECT de datos taste_testmain' . pg_result_error());
			if ($this->rs)
			{
				$resulsql = @pg_fetch_array($this->rs,0);
				if ($prmt=='1') return $resulsql[7];
				if ($prmt=='2') return $resulsql[2];
				if ($prmt=='3') return $resulsql[3];
				if ($prmt=='4') return $resulsql[4];
				if ($prmt=='5') return $resulsql[5];
			}
			else
			{
				echo 'no se conecto';
			}	
			$resulsql = pg_fetch_all($this->rs);
			print_r ($resulsql);
		}
		
		//metodo para busqueda del Juez Sensorial
		function findJuezsensorial($codigo)
		{
			$sql="SELECT iduser_usrm, pnuser_usrm, snuser_usrm, pauser_usrm, sauser_usrm  FROM public.taste_user_m WHERE iduser_usrm='$codigo' ORDER BY pnuser_usrm ASC";
        
			$this->rs=pg_query($sql) or die ('ERROR AL HACER EL SELECT DEL Juez Sensorial' . pg_result_error());
			if ($this->rs)
			{
				$resulsql = @pg_fetch_array($this->rs,0);
				$nombrejs=$resulsql[1].' '.$resulsql[2].' '.$resulsql[3].' '.$resulsql[4];
				return $nombrejs;
			}
			else
			{
				echo 'no se conecto';
			}	
			$resulsql = pg_fetch_all($this->rs);
			print_r ($resulsql);
		}
		
		//metodo para actualizar el Juez Sensorial para ECS
		function updateJuezsensorial($codigo, $codjs)
		{
			$sql="UPDATE public.taste_testmain SET tm_codjs='$codjs' WHERE tm_codgral='$codigo'";
        
			$this->rs=pg_query($sql) or die ('ERROR AL HACER EL SELECT DEL Juez Sensorial' . pg_result_error());
			if ($this->rs)
			{
				return '1';
			}
			else
			{
				return '0';
			}	
			$resulsql = pg_fetch_all($this->rs);
			print_r ($resulsql);
		}
		
		
		//metodo para listar las evaluaciones de calidad sensorial taste_testmain
		function findEcslist($status1)
		{
			if ($status1==1) {$sql="SELECT tm_codgral, tm_tipmst, tm_codjs, tm_codv1, tm_codv2, tm_codv3, tm_status, tm_descrip  FROM public.taste_testmain";}
			if ($status1==2) {$sql="SELECT tm_codgral, tm_tipmst, tm_codjs, tm_codv1, tm_codv2, tm_codv3, tm_status, tm_descrip  FROM public.taste_testmain WHERE tm_status='0'";}
			if ($status1==3) {$sql="SELECT tm_codgral, tm_tipmst, tm_codjs, tm_codv1, tm_codv2, tm_codv3, tm_status, tm_descrip  FROM public.taste_testmain WHERE tm_status='1'";}
			
			echo $sql;
			 
			$this->rs=pg_query($sql) or die ('ERROR AL HACER EL SELECT DE la ECS pos su estatus' . pg_result_error());
			for($i = 1; $arreglo[$i] = @pg_fetch_array($this->rs); $i++);
			return $arreglo;
		}
		
		//metodo para listar las evaluaciones de calidad sensorial taste_testmain discriminando por Juez Sensorial
		function findEcspend($codigo)
		{
			$sql="SELECT tm_codgral, tm_tipmst, tm_codjs, tm_codv1, tm_codv2, tm_codv3, tm_status, tm_descrip  FROM public.taste_testmain WHERE tm_codjs='$codigo'";
			
			$this->rs=pg_query($sql) or die ('ERROR AL HACER EL SELECT DE la ECS pos su el juez sensorial' . pg_result_error());
			for($i = 1; $arreglo[$i] = @pg_fetch_array($this->rs); $i++);
			return $arreglo;
		}
		
		//metodo para buscar las instrucciones para las  evaluaciones de calidad sensorial taste_testmain
		function findInstrucciones($codigo)
		{
			$sql="SELECT idevalscm_ttm, descecscm_ttm, instecscm_ttm FROM public.taste_tasting_m WHERE idevalscm_ttm='$codigo'";
			
			$this->rs=pg_query($sql) or die ('ERROR AL HACER EL SELECT DE las instrucciones' . pg_result_error());
			if ($this->rs)
			{
				$resulsql = @pg_fetch_array($this->rs,0);
				return $resulsql[2];
			}
			else
			{
				return '0';
			}	
			$resulsql = pg_fetch_all($this->rs);
			print_r ($resulsql);
		}
		
		//metodo para listar las evaluaciones de calidad sensorial taste_testmain discriminando por codigo de evaluacion sensorial
		function findEcsevaluar($codigo)
		{
			$sql="SELECT tm_codgral, tm_tipmst, tm_codjs, tm_codv1, tm_codv2, tm_codv3, tm_status, tm_descrip  FROM public.taste_testmain WHERE tm_codgral='$codigo'";
			
			$this->rs=pg_query($sql) or die ('ERROR AL HACER EL SELECT DE la ECS pos su el codigo de la ECS' . pg_result_error());
			for($i = 1; $arreglo[$i] = @pg_fetch_array($this->rs); $i++);
			return $arreglo;
		}
		
		//hace el insert en la base de datos para la evaluacion de cerveza 28  (cabecera) taste_testcp28g_c
		function recordEvaluarc28($codigo,$tipo,$codjs, $vaso1, $vaso2, $vaso3, $status,$descecs)
		{
			$sql="INSERT INTO public.taste_testcp28g_c(idtestev_ttc, idmstgral_ttc, idjuesen_ttc, fectestev_ttc, horatestev_ttc) VALUES (?, ?, ?, ?, ?)";
			$this->rs=pg_query($sql) or die ('ERROR AL HACER EL INSERT de datos taste_testmain' . pg_result_error());
			if ($this->rs){$resulsqlm=1;}
			else{$resulsqlm=0;}
			return $resulsqlm;
		}
		
		//hace el insert en la base de datos para la evaluacion de cerveza 28  (detalle) taste_testcp28g_c
		function recordEvaluard28($codEcs, $vasoEcs, $cbx_est, $cbx_lev, $cbx_pan, $cbx_dulce, $cbx_cara, $cbx_mfds, $cbx_azu, $cbx_luz, $cbx_cuero, $cbx_pacarton, $cbx_acidoso, $cbx_grc, $cbx_rancio, $cbx_metalico, $cbx_vino, $cbx_madera, $cbx_otros, $resultEcs1, $cbx_asp, $cbx_pr, $cbx_rem, $cbx_ast, $cbx_otr, $resultEcs2, $resultEcs3, $txt_lp)
		{
			$sql="INSERT INTO public.taste_testcp28g_t(
            idtestev_ttt, cdvatestev_ttt, go_es_cp28g_ttt, go_lv_cp28g_ttt, 
            go_pan_cp28g_ttt, go_dc_cp28g_ttt, go_crm_cp28g_ttt, go_mfds_cp28g_ttt, 
            go_azu_cp28g_ttt, go_luz_cp28g_ttt, go_cue_cp28g_ttt, go_pac_cp28g_ttt, 
            go_aci_cp28g_ttt, go_grc_cp28g_ttt, go_ran_cp28g_ttt, go_met_cp28g_ttt, 
            go_vs_cp28g_ttt, go_mad_cp28g_ttt, go_otros_cp28g_ttt, go_total_cp28g_ttt, 
            ca_as_cp28g_ttt, ca_pr_cp28g_ttt, ca_re_cp28g_ttt, ca_ast_cp28g_ttt, 
            ca_otros_cp28g_ttt, ca_total_cp28g_ttt, eo_total_cp28g_ttt, lp_cp28g_ttt)
			VALUES ('$codEcs', '$vasoEcs', ' $cbx_est', ' $cbx_lev', ' $cbx_pan', '$cbx_dulce', '$cbx_cara', '$cbx_mfds', '$cbx_azu', '$cbx_luz', '$cbx_cuero', '$cbx_pacarton', '$cbx_acidoso', '$cbx_grc', '$cbx_rancio', '$cbx_metalico', '$cbx_vino', '$cbx_madera', '$cbx_otros', '$resultEcs1', '$cbx_asp', '$cbx_pr', '$cbx_rem', '$cbx_ast', '$cbx_otr', '$resultEcs2', '$resultEcs3', '$txt_lp')";
			$this->rs=pg_query($sql) or die ('ERROR AL HACER EL INSERT de datos taste_testmain' . pg_result_error());
			if ($this->rs){$resulsqlm=1;}
			else{$resulsqlm=0;}
			return $resulsqlm;
		}
		
		//hace el update de la base de datos de la evaluacion de calidad sensorial taste_testmain
		function updateStatustest($codigo)
		{
			$sql="UPDATE public.taste_testmain SET tm_status='1' WHERE tm_codgral='$codigo'";
        
			$this->rs=pg_query($sql) or die ('ERROR AL HACER EL SELECT DEL Juez Sensorial' . pg_result_error());
			if ($this->rs)
			{
				return '1';
			}
			else
			{
				return '0';
			}	
			$resulsql = pg_fetch_all($this->rs);
			print_r ($resulsql);
		}
		
		/*
		************************************************************************************
		************************************************************************************
				SECCION PARA EL MANEJO DE LOS SQL RELACIONADOS CON LOS USUARIOS
		************************************************************************************
		************************************************************************************
		*/
		
		function recordUsuario($tx_username, $tx_pwd,$cbx_tipoUsuario, $tx_pnuser, $tx_snuser, $tx_pauser, $tx_sauser, $tx_ciuser, $tx_dir1user, $tx_dir2user, $tx_tlf1user, $tx_tlf2user, $tx_tlf3user, $tx_emailuser)
		{
			$sql="INSERT INTO public.taste_user_m (iduser_usrm, nuser_usrm, pwduser_usrm, pnuser_usrm, snuser_usrm, pauser_usrm, sauser_usrm, ciuser_usrm, codcar_usrm, dir1user_usrm, dir2user_usrm, tlf1user_usrm, tlf2user_usrm, tlf3user_usrm, emailuser_usrm, status_usrm) VALUES ('$tx_ciuser', '$tx_username', '$tx_pwd', '$tx_pnuser', '$tx_snuser', '$tx_pauser', '$tx_sauser', '$tx_ciuser', '$cbx_tipoUsuario','$tx_dir1user', '$tx_dir2user', '$tx_tlf1user', '$tx_tlf2user', '$tx_tlf3user', '$tx_emailuser', '1')";
			
			$this->rs=pg_query($sql) or die ('ERROR AL HACER EL INSERT de datos taste_user_m' . pg_result_error());
			if ($this->rs){$resulsqlm=1;}
			else{$resulsqlm=0;}
			return $resulsqlm;
		}
			
		function selectUsuario($codigo)
		{
			$sql="SELECT iduser_usrm, nuser_usrm, pwduser_usrm, pnuser_usrm, snuser_usrm, pauser_usrm, sauser_usrm, ciuser_usrm, codcar_usrm, dir1user_usrm, dir2user_usrm, tlf1user_usrm, tlf2user_usrm, tlf3user_usrm, emailuser_usrm FROM public.taste_user_m WHERE iduser_usrm='$codigo'";
        
			$this->rs=pg_query($sql) or die ('ERROR AL HACER EL SELECT DE LOS USUARIOS DEL SYSTEMA' . pg_result_error());
			for($i = 1; $arreglo[$i] = @pg_fetch_array($this->rs); $i++);
			return $arreglo;
		
		}
		
		function updateUsuario($tx_username, $cbx_tipoUsuario, $tx_pnuser, $tx_snuser, $tx_pauser, $tx_sauser, $tx_dir1user, $tx_dir2user, $tx_tlf1user, $tx_tlf2user, $tx_tlf3user, $tx_emailuser)
		{
			$sql="UPDATE public.taste_user_m SET pnuser_usrm='$tx_pnuser', snuser_usrm='$tx_snuser' pauser_usrm='$tx_pauser', sauser_usrm=' $tx_sauser', codcar_usrm='$cbx_tipoUsuario', dir1user_usrm='$tx_dir1user', dir2user_usrm='$tx_dir2user', tlf1user_usrm='$tx_tlf1user', tlf2user_usrm='$tx_tlf2user', tlf3user_usrm='$tx_tlf3user', emailuser_usrm='$tx_emailuser' WHERE iduser_usrm='$tx_username'";
        
			$this->rs=pg_query($sql) or die ('ERROR AL HACER EL update de los usuarios de la app' . pg_result_error());
			if ($this->rs)
			{
				return '1';
			}
			else
			{
				return '0';
			}	
			$resulsql = pg_fetch_all($this->rs);
			print_r ($resulsql);
		}
		
		function updateEstatususuario($cbx_appUsuario, $cbx_statusUsuario)
		{
			$sql="UPDATE public.taste_user_m SET  status_usrm='$cbx_statusUsuario' WHERE iduser_usrm='$cbx_appUsuario'";
        
			$this->rs=pg_query($sql) or die ('ERROR AL HACER EL update del status de lo usuarios de la app' . pg_result_error());
			if ($this->rs)
			{
				return '1';
			}
			else
			{
				return '0';
			}	
			$resulsql = pg_fetch_all($this->rs);
			print_r ($resulsql);
		}
		
		function updateResetpassword($cbx_appUsuario, $pwdUser)
		{
			$sql="UPDATE public.taste_user_m SET  pwduser_usrm='$pwdUser' WHERE iduser_usrm='$cbx_appUsuario'";
        
			$this->rs=pg_query($sql) or die ('ERROR AL HACER EL update del status de lo usuarios de la app' . pg_result_error());
			if ($this->rs)
			{
				return '1';
			}
			else
			{
				return '0';
			}	
			$resulsql = pg_fetch_all($this->rs);
			print_r ($resulsql);
		}
		
		/*
		*******************************************************************************************
		*******************************************************************************************
		 SECCION PARA EL MANEJO DE LOS SQL RELACIONADOS A LA MANI[PULACION DE LOS TIPOS DE MUESTRA
		*******************************************************************************************
		*******************************************************************************************
		*/
		function findCodigomuestranew()
		{
			$sql="SELECT idevalscm_ttm, descecscm_ttm, instecscm_ttm FROM public.taste_tasting_m ORDER BY idevalscm_ttm ASC";
			
			$this->rs=pg_query($sql) or die ('ERROR AL HACER EL SELECT DE datos taste_tasting_m' . pg_result_error());
			for($i = 1; $arreglo[$i] = @pg_fetch_array($this->rs); $i++);
			return $arreglo;
		}
			
			
		function insertTipomuestras($codigo, $descTipomst, $instruc)
		{
			$sql="INSERT INTO public.taste_tasting_m(idevalscm_ttm, descecscm_ttm, instecscm_ttm) VALUES ('$codigo', '$descTipomst', '$instruc')";
			
			$this->rs=pg_query($sql) or die ('ERROR AL HACER EL INSERT de datos taste_tasting_m' . pg_result_error());
			if ($this->rs){$resulsqlm=1;}
			else{$resulsqlm=0;}
			return $resulsqlm;
		}
		
		function updateTipomuestras($codigo,$descTipomst, $instruc)
		{
			$sql="UPDATE public.taste_tasting_m  SET descecscm_ttm='$descTipomst', instecscm_ttm='$instruc' WHERE idevalscm_ttm='$codigo'";
        
			$this->rs=pg_query($sql) or die ('ERROR AL HACER EL update de la informacion del tipo de muestra' . pg_result_error());
			if ($this->rs)
			{
				return '1';
			}
			else
			{
				return '0';
			}	
			$resulsql = pg_fetch_all($this->rs);
			print_r ($resulsql);
		}
		
		function updateTipomuestrasaod($codigo, $statusMst)
		{
			$sql="UPDATE public.taste_tasting_m SET status_ttm='$statusMst' WHERE idevalscm_ttm='$codigo'";
        
			$this->rs=pg_query($sql) or die ('ERROR AL HACER EL update de la informacion del tipo de muestra' . pg_result_error());
			if ($this->rs)
			{
				return '1';
			}
			else
			{
				return '0';
			}	
			$resulsql = pg_fetch_all($this->rs);
			print_r ($resulsql);
		}
		
		
		/*
		************************************************************************************
		************************************************************************************
							SECCION PARA LOS SELECT DE LOS COMBOBOX
		************************************************************************************
		************************************************************************************
		*/
		
		//metodo para busqueda en el maestro de tipos de muestras y devuelve un array con la informacion
		function findTipomuestras()
		{
			//sql para extraer la informacion de la descripcion del cargo del maestro de cargos 
			$sql="SELECT idevalscm_ttm, descecscm_ttm, instecscm_ttm FROM taste_tasting_m ORDER BY descecscm_ttm ASC";
        
			$this->rs=pg_query($sql) or die ('ERROR AL HACER EL SELECT DE LOS TIPOS DE MUESTRAS' . pg_result_error());
			for($i = 1; $arreglo[$i] = @pg_fetch_array($this->rs); $i++);
			return $arreglo;
		}
		
		//metodo para busqueda en el maestro de tipos de muestras y devuelve un array con la informacion
		function cbxMuestrageneral()
		{
			$sql="SELECT id_carga_mst, desc_carga_mst FROM taste_mst_cargac ORDER BY desc_carga_mst ASC";
        
			$this->rs=pg_query($sql) or die ('ERROR AL HACER EL SELECT DE LOS TIPOS DE MUESTRAS' . pg_result_error());
			for($i = 1; $arreglo[$i] = @pg_fetch_array($this->rs); $i++);
			return $arreglo;
		}

		//metodo para busqueda del Juez Sensorial y devuelve un array con la informacion
		function cbxJuezsensorial()
		{
			$sql="SELECT iduser_usrm, pnuser_usrm, snuser_usrm, pauser_usrm, sauser_usrm  FROM public.taste_user_m WHERE codcar_usrm='2' ORDER BY pnuser_usrm ASC";
        
			$this->rs=pg_query($sql) or die ('ERROR AL HACER EL SELECT DE LOS TIPOS DE MUESTRAS' . pg_result_error());
			for($i = 1; $arreglo[$i] = @pg_fetch_array($this->rs); $i++);
			return $arreglo;
		}
		
		//metodo para busqueda de los numero de vaso para la evaluacion sensorial 
		function cbxVasos($codigo)
		{
			$sql="SELECT mstcv_id, mstcv_codgral, mstcv_nvaso   FROM public.taste_mst_cargav WHERE mstcv_codgral = $codigo and mstcv_status = 0 ORDER BY  mstcv_id ASC";
			
			$this->rs=pg_query($sql) or die ('ERROR AL HACER EL SELECT DE LOS CODIGOS DE LOS VASOS' . pg_result_error());
			//return $this->rs;
			for($i = 1; $arreglo[$i] = @pg_fetch_array($this->rs); $i++);
			return $arreglo;
		}
		
		//metodo para busqueda de las evaluacion de calidad sensorial 
		function cbxEcs()
		{
			$sql="SELECT tm_codgral, tm_tipmst, tm_codjs, tm_codv1, tm_codv2, tm_codv3, tm_status, tm_descrip  FROM public.taste_testmain ORDER BY tm_descrip ASC";
        
			$this->rs=pg_query($sql) or die ('ERROR AL HACER EL SELECT DE LOS CODIGOS DE LOS VASOS' . pg_result_error());
			for($i = 1; $arreglo[$i] = @pg_fetch_array($this->rs); $i++);
			return $arreglo;
		}
		
		//metodo para busqueda de los usuarios del sistemas
		function cbxUsuario()
		{
			$sql="SELECT iduser_usrm, nuser_usrm, pwduser_usrm, pnuser_usrm, snuser_usrm, pauser_usrm, sauser_usrm, ciuser_usrm, codcar_usrm, dir1user_usrm, dir2user_usrm, tlf1user_usrm, tlf2user_usrm, tlf3user_usrm, emailuser_usrm FROM public.taste_user_m ORDER BY nuser_usrm ASC";
        
			$this->rs=pg_query($sql) or die ('ERROR AL HACER EL SELECT DE LOS USUARIOS DEL SYSTEMA' . pg_result_error());
			for($i = 1; $arreglo[$i] = @pg_fetch_array($this->rs); $i++);
			return $arreglo;
		}
		
	}
?>
