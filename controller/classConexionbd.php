<?php
	require ("config.php");

	class conectarDb
	{
		var $host;
		var $port;
		var $db;
		var $user;
		var $pwd;
		var $connect;
                
		public function __construct ()
		{
			$this->host=SERVIDOR;
			$this->db=BD;
			$this->port=PUERTO;
			$this->user=USUARIO;
			$this->pwd=PASS;
			$this->connect=$this->conectarDb();
			//$this->conectarDb($this->host,$this->port,$this->db,$this->user,$this->pwd);
		}
				
		function conectarDb()
		{
			$strCnx="host=$this->host port=$this->port dbname=$this->db  user=$this->user password=$this->pwd";
			//echo $strCnx;
			$rs1=pg_pconnect($strCnx) or die ("conexion fallida");
			return $rs1;
		}
                
		function __destruct() {$this->cerrar_db();}
                
		function cerrar_db() {pg_close();}
     }    
?>
