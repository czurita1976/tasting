<?php
	class manejaAcceso
	{
		function loginUsuario($boton, $usuario, $clave, $tipod) //metodo para el manejo de acceso del usuario a la APP MOVIL
		{
			if ($boton=='BT_ACEPTAR') //metodo para el boton aceptar
			{
				$conndb=new DAOsql();
				if ($tipod='1') //devuelve el nombre del usuario con el parametro 1
				{
					$result1=$conndb->accesoLogin($usuario, $clave, $tipod);
					return $result1;
				}
			}
			if ($parameter=='BT_CANCELAR') //metodo para elmanejo del boton cancelar
			{
				echo'USTED DECIDIO CANCELAR LA SESION';
			}
		}
		
		function cargoUsuario($boton, $usuario, $clave, $tipod) //metodo para el manejo de acceso del usuario a la APP MOVIL
		{
			if ($boton=='BT_ACEPTAR') //metodo para el boton aceptar
			{
				$conndb=new DAOsql();
				if ($tipod='2') //devuelve el codigo del cargo del usuario con el parametro 2
				{
					$result2=$conndb->cargoUser($usuario, $clave, $tipod);
					return $result2;
				}
			}
			if ($parameter=='BT_CANCELAR') //metodo para elmanejo del boton cancelar
			{
				echo'USTED DECIDIO CANCELAR LA SESION';
			}
		}
		
		function descriptionCargo($codCar)
		{
			$conndb=new DAOsql();
			$result3=$conndb->descriptionUser($codCar);
			return $result3;
		}
				
		function informacionUsuario()
		{
			echo ("<script  language='javascript'>
					self.location ='../ui/userInformation.php';
				</script>");
		}
	}
?>
