<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
	<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>MENU PRINCIPAL</title>
		<link href="../bootstrap/css/bootstrap.min.css" rel="stylesheet">
	</head>
	<body>
		<nav class="navbar navbar-default">
		  <div class="container-fluid">
			<!-- Brand and toggle get grouped for better mobile display -->
			<div class="navbar-header">
			  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			  </button>
			  <a class="navbar-brand" href="#">TASTING POLAR</a>
			</div>

			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			  <ul class="nav navbar-nav">
				<li class="dropdown">
				  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">ACTIVIDAD<span class="caret"></span></a>
				  <ul class="dropdown-menu">
					<li><a href=''>VISTA PRELIMINAR</a></li>
					<li><a href=''>TRANSFERENCIA A FORMATO PDF</a></li>
					<li><a href=''>IMPRIMIR</a></li>
					<li><a href=''>SALIR</a></li>
				  </ul>
				</li>
			  </ul>
			  
			 
			  <ul class="nav navbar-nav">
				<li class="dropdown">
				  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">ANALISTA<span class="caret"></span></a>
				  <ul class="dropdown-menu">
					<li><a href='../ui/registraMuestras.php'>REGISTRO GRAL. MUESTRA</a></li>
					<li><a href='../ui/consultaMuestra.php'>CONSULTA GRAL. MUESTRAS</a></li>
					<li><a href='../ui/registraEcs.php'>REGISTRO DE ECS</a></li>
					<li><a href='../ui/consultaEcs.php'>CONSULTA DE ECS</a></li>
					<li><a href='../ui/actualizarEcs.php'>ACTUALIZAR ECS</a></li>
					<li><a href='../ui/listarEcs.php'>LISTAR ECS</a></li>
				  </ul>
				</li>
			  </ul>
			  
			  		  
			  <ul class="nav navbar-nav">
				<li class="dropdown">
				  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">JUECES SENSORIALES<span class="caret"></span></a>
				  <ul class="dropdown-menu">
					<li><a href=''>EVALUAR ECS</a></li>
				  </ul>
				</li>
			  </ul>
			  
			  			  
			  <ul class="nav navbar-nav">
				<li class="dropdown">
				  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">ROOT-ADMIN<span class="caret"></span></a>
				  <ul class="dropdown-menu">
					<li><a href=''>REGISTRO DE USUARIO</a></li>
						<li><a href=''>ACTUALIZAR USUARIO</a></li>
						<li><a href=''>ACTIVAR O DESACTIVAR USUARIO</a></li>
						<li><a href=''>REGISTRO DE TIPO DE MUESTRA</a></li>
						<li><a href=''>ACTUALIZAR TIPO DE MUESTRA</a></li>
						<li><a href=''>ACTIVAR O DESACTIVAR TIPO DE MUESTRA</a></li>
						<li><a href=''>LISTAR ACTIVIDADES DE USUARIO</a></li>
				  </ul>
				</li>
			  </ul>
			  
			 		  
			  <form class="navbar-form navbar-left">
				<div class="form-group">
				  <input type="text" class="form-control" placeholder="Buscar">
				</div>
				<button type="submit" class="btn btn-default">Aceptar</button>
			  </form>
			  
			  
			</div><!-- /.navbar-collapse -->
			
			
		</nav>
	</body>
</form>

<script language="javascript" src=../js/jquery-3.2.1.min.js></script>
<script language="javascript" src=../bootstrap/js/bootstrap.min.js></script>
