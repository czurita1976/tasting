<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
	<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>TASTE POLAR</title>
		
	</head>
	<body>
	
	<div id='contenedor'>
		<div id='header'>
			<ul class='nav'>
				<li><a href=''>ACTIVIDADES</a>
					<ul>
						<li><a href=''>TRANSFERENCIA SAP-ERP</a>
							<ul>
								<li><a href=''>GENERAR EL ARCHIVO PLANO</a></li>
							</ul>
						</li>
						<li><a href=''>VISTA PRELIMINAR</a></li>
						<li><a href=''>TRANSFERENCIA A FORMATO PDF</a></li>
						<li><a href=''>IMPRIMIR</a></li>
						<li><a href=''>SALIR</a></li>
					</ul>
				</li>
				<li><a href=''>ANALISTA</a>
					<ul>
						<li><a href='../ui/registraMuestras.php'>REGISTRO GRAL. MUESTRA</a></li>
						<li><a href='../ui/consultaMuestra.php'>CONSULTA GRAL. MUESTRAS</a></li>
						<li><a href='../ui/registraEcs.php'>REGISTRO DE ECS</a></li>
						<li><a href='../ui/consultaEcs.php'>CONSULTA DE ECS</a></li>
						<li><a href='../ui/actualizarEcs.php'>ACTUALIZAR ECS</a></li>
						<li><a href='../ui/listarEcs.php'>LISTAR ECS</a></li>
					</ul>

				</li>
				<li><a href=''>JUECES SENSORIALES</a>
					<ul>
						<li><a href=''>EVALUAR ECS</a></li>
					</ul>

				</li>
				<li><a href=''>ROOT-ADMIN</a>
					<ul>
						<li><a href=''>REGISTRO DE USUARIO</a></li>
						<li><a href=''>ACTUALIZAR USUARIO</a></li>
						<li><a href=''>ACTIVAR O DESACTIVAR USUARIO</a></li>
						<li><a href=''>REGISTRO DE TIPO DE MUESTRA</a></li>
						<li><a href=''>ACTUALIZAR TIPO DE MUESTRA</a></li>
						<li><a href=''>ACTIVAR O DESACTIVAR TIPO DE MUESTRA</a></li>
						<li><a href=''>LISTAR ACTIVIDADES DE USUARIO</a></li>
					</ul>

				</li>
			</ul>
		</div>	
	  </div>
	</body>
</form>
