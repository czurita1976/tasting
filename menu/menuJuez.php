<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
	<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>TASTE POLAR</title>
		<style type='text/css'>
			
			* 
			{
				padding:0px;
				margin:0px;	
			}
			
			#contenedor
			{
				position:relative;
			}
			
			#header
			{
				position:absolute;
				left:50%;
				margin:auto;
				width:150%;
				font-family:Arial, Helvetica, sans-serif;
			}
		
			ul, ol 
			{
				list-style:none;
			}
			
			.nav li a 
			{
				background-color:#000;
				color:#fff;
				text-decoration:none;
				padding:10px 15px;
				display:block;
			}
			
			.nav li a:hover
			{
				backgroung-color:#434343;
			}
			
			.nav > li
			{
				float:left;
			}
			
			.nav li ul 
			{
				display:none;
				position:absolute;
				min-width:140px;
			}
			
			.nav li:hover > ul
			{
				display:block;
			}
			
			.nav li ul li 
			{
				position:relative;
			}
			
			.nav li ul li ul
			{
				right:-250px;
				top:0px;
			}
			
			
			
		</style>
	</head>
	<body>
	<tr>
  		<td align="center" style="height:35px;"><h4>Pantalla de servicios (P-3)</h4></td>
  	</tr>
	<div id='contenedor'>
		<div id='header'>
			<ul class='nav'>
				<li><a href=''>ACTIVIDADES</a>
					<ul>
						<li><a href=''>TRANSFERENCIA SAP-ERP</a>
							<ul>
								<li><a href=''>GENERAR EL ARCHIVO PLANO</a></li>
							</ul>
						</li>
						<li><a href=''>VISTA PRELIMINAR</a></li>
						<li><a href=''>TRANSFERENCIA A FORMATO PDF</a></li>
						<li><a href=''>IMPRIMIR</a></li>
						<li><a href=''>SALIR</a></li>
					</ul>
				</li>
				<li><a href=''>ANALISTA</a>
					<ul>
						<li><a href='../POLAR/ANALISTA/cargaMuestras.php'>REGISTRO INICIAL MUESTRA</a></li>
						<li><a href='../POLAR/ANALISTA/verificaMuestras.php'>CONSULTAR MUESTRAS</a></li>
						<li><a href='../POLAR/ANALISTA/cargaEsc.php'>REGISTRO DE ECS</a></li>
						<li><a href='../POLAR/ANALISTA/verificaEsc.php'>CONSULTA DE ECS</a></li>
						<li><a href='../POLAR/ANALISTA/modificarEsc.php'>ACTUALIZAR ECS</a></li>
						<li><a href='../POLAR/ANALISTA/listadoEsc.php'>LISTAR ECS</a></li>
					</ul>

				</li>
				<li><a href=''>JUECES SENSORIALES</a>
					<ul>
						<li><a href='../JUEZ/userInformation.php'>EVALUAR ECS</a></li>
					</ul>

				</li>
				<li><a href=''>ROOT-ADMIN</a>
					<ul>
						<li><a href='../POLAR/ROOT/crearUsuario.php'>REGISTRO DE USUARIO</a></li>
						<li><a href='../POLAR/ROOT/actualizarUsusario.php'>ACTUALIZAR USUARIO</a></li>
						<li><a href='../POLAR/ROOT/actdesUsuario.php'>ACTIVAR O DESACTIVAR USUARIO</a></li>
						<li><a href='../POLAR/ROOT/crearEcs.php'>REGISTRO DE TIPO DE MUESTRA</a></li>
						<li><a href='../POLAR/ROOT/actualizarEsc.php'>ACTUALIZAR TIPO DE MUESTRA</a></li>
						<li><a href='../POLAR/ROOT/actdesEsc.php'>ACTIVAR O DESACTIVAR TIPO DE MUESTRA</a></li>
						<li><a href='../POLAR/ROOT/reporteActividades.php'>LISTAR ACTIVIDADES DE USUARIO</a></li>
					</ul>

				</li>
			</ul>
		</div>	
	  </div>
	</body>
</form>
