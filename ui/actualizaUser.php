<?php 
	require ("../controller/classDAO.php");
	$DAOsql=new DAOsql;
	$resultado=$DAOsql->cbxUsuario();
?>							
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<div id="ccs-header" align="center"><?php require("../menu/menuMain.php");?></div>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>TASTE POLAR</title>
    <link rel='stylesheet' href=' '>
</head>
<body>
<form id="actualizaUser" name="actualizaUser" method="post" action="../controller/classMain.php">
  
  <table class="DashPartTitle4" width="100%">
  	<tr class="">
    	<td align="center" ></td>
   	</tr>
  	<tr>
  		<td align="center" style="height:35px;"></td>
  	</tr>
	<tr>
  		<td align="center" style="height:35px;"><h4>Pantalla para la actualizacion de la informacion del usuarios en la aplicacion (P-14)</h4></td>
  	</tr>	
  	<tr>
  		<td height="46" align="center">
        	<table align="center" border="1">
			    
			    <tr>
                	<td>USUARIO DE LA APP: </td>
                	<td>
						<select id="cbx_appUsuario" name ="cbx_appUsuario">
							<option value="0">Seleccione el Usuario de la APP</option>
							<?php 
								$contar=0;
								foreach ($resultado as $row){ 
								if ($contar!=count($row)-1)
								{?> 
								<option value="<?php echo $row['iduser_usrm'];?>"><?php echo $row['nuser_usrm'];?></option>
							<?php }} 
								unset($row);
							?>
						</select>
					</td>		
                </tr>
                <?php
					$resultado1=$DAOsql->selectUsuario();
					foreach ($resultado1 as $row1)
					{ 
						$pnUsuario=$row1['pnuser_usrm'];
						$snUsuario=$row1['snuser_usrm'];
						$paUsuario=$row1['pauser_usrm'];
						$saUsuario=$row1['sauser_usrm'];
						$codCargo=$row1['codcar_usrm'];
						$descripCargo=$DAOsql->descriptionUser($codCargo);
						$dir1Usuario=$row1['dir1user_usrm']; 
						$dir1Usuario=$row1['dir2user_usrm']; 
						$tlf1Usuario=$row1['tlf1user_usrm']; 
						$tlf2Usuario=$row1['tlf2user_usrm']; 
						$tlf3Usuario=$row1['tlf3user_usrm']; 
						$emailUsuario=$row1['emailuser_usrm']; 
					} 
					unset($row);
                ?>
                <tr>
                	<td>PRIMER NOMBRE: </td>
                	<td><?phpecho $pnUsuario;?></td>
                	<td><input type="text" name="txt_muestrad" id="txt_muestrad" style="width:300px"/></td>
                </tr>
                <tr>
                	<td>SEGUNDO NOMBRE: </td>
                	<td><?phpecho $snUsuario;?></td>
                    <td><input type="text" name="tx_snuser" id="tx_snuser" style="width:300px"/></td>
                </tr>
                <tr>
                	<td>PRIMER APELLIDO: </td>
                	<td><?phpecho $paUsuario;?></td>
                    <td><input type="text" name="tx_pauser" id="tx_pauser" style="width:300px"/></td>
                </tr>
                <tr>
                	<td>SEGUNDO APELLIDO: </td>
                	<td><?phpecho $saUsuario;?></td>
                    <td><input type="text" name="tx_sauser" id="tx_sauser" style="width:300px"/></td>
                </tr>    
                <tr>
                	<td>CARGO: </td>
                	<td><?phpecho $descripCargo;?></td>
                    <td>
        				<select id="cbx_tipoUsuario" name="cbx_tipoUsuario">
							<option value="0">Seleccione el Tipo de Usuario</option>
							<option value="1">ANALISTA</option>
							<option value="2">jUEZ SENSORIA</option>
							<option value="3">ADMINISTRADOR</option>
						</select>
					</td>
                </tr>
            	<tr>
                	<td>DIRECCION PRINCIPAL: </td>
                	<td><?phpecho $dir1Usuario;?></td>
                    <td><input type="text" name="tx_dir1user" id="tx_dir1user" style="width:300px"/></td>
                </tr>
                <tr>
                	<td>DIRECCION SECUNDARIA: </td>
                	<td><?phpecho $dir1Usuario;?></td>
                    <td><input type="text" name="tx_dir2user" id="tx_dir2user" style="width:300px"/></td>
                </tr>
                <tr>
                	<td>TELEFONO # 1: </td>
                	<td><?phpecho $tlf1Usuario;?></td>
                    <td><input type="text" name="tx_tlf1user" id="tx_tlf1user" style="width:300px"/></td>
                </tr>
                <tr>
                	<td>TELEFONO # 2: </td>
                	<td><?phpecho $tlf2Usuario;?></td>
                    <td><input type="text" name="tx_tlf2user" id="tx_tlf2user" style="width:300px"/></td>
                </tr>
                <tr>
                	<td>TELEFONO # 3: </td>
                	<td><?phpecho $tlf3Usuario;?></td>
                    <td><input type="text" name="tx_tlf3user" id="tx_tlf3user" style="width:300px"/></td>
                </tr>
            	<tr>
                	<td>CORREO ELECTRONICO: </td>
                	<td><?phpecho $emailUsuario;?></td>
                    <td><input type="text" name="tx_emailuser" id="tx_emailuser" style="width:300px"/></td>
                </tr>
           </table>
  	</tr>
    <tr>
    	<td height="46"	 align="center">
        	<table align="center">
            	<tr>
                	<td><input type="submit" name="btna_actuserfrm" id="btna_ruserfrm" value="ACEPTAR" /></td>
                    <td><input type="submit" name="btnc_actuserfrm" id="btnc_ruserfrm" value="CANCELAR" /></td>
                </tr>
            </table>
        </td>
     </tr>               
  </table>
</form>
