<?php 
	require ("../controller/classDAO.php");
	$DAOsql=new DAOsql;
	$resultado=$DAOsql->cbxMuestrageneral();
	//var_dump ($resultado);
?>							
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<div id="ccs-header" align="center"><?php require("../menu/menuMain.php");?></div>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>TASTE POLAR</title>
    <script language="javascript" src=../js/jquery-3.2.1.min.js></script>
    
    <link rel='stylesheet' href=' '>
</head><body>
<form id="cargaMuestras" name="cargaMuestras" method="post" action="../controller/classMain.php">
  
  <table class="DashPartTitle2" width="100%">
  	<tr class=""><td align="center" ></td></tr>
  	<tr><td align="center" style="height:35px;"></td></tr>
  	<tr><td align="center" style="height:35px;"><h4>Pantalla para la consulta de muestras generales (P-5)</h4></td></tr>

  	<tr>
  		<td height="46" align="center">
        	<table align="center" border="1">
				<tr>
                	<td>DESCRIPCION GENERAL DE LA MUESTRA</td>
                	<td>
						<select id="cbx_cargamuestra" name ="cbx_cargamuestra">
							<option value="0">Seleccione la Muestra General</option>
							<?php 
								$contar=0;
								foreach ($resultado as $row){ 
								if ($contar!=count($row)-1)
								{?>
								<option value="<?php echo $row['id_carga_mst'];?>"><?php echo $row['desc_carga_mst'];?></option>
							<?php }} 
								unset($row);
							?>
						</select>	
					</td>
        		</tr>
		
                </tr>
            	
            
            	<table align="center" border="2">
					<tr>
						<td>TIPO DE EVALUACION DE CALIDAD SENSORIAL</td>
						<td>
							<?php 
								echo $_SESSION['descripTipomuestra'];
							?>
						</td>
					</tr>
				</table>
            
            	<table align="center" border="2">
					<tr>
						<td>                                                                    </td>
					</tr>    
            	</table>
            
                <table align="center" border="2">
					<tr>
						<td>MUESTRA # 1</td>
						<td><?php echo $_SESSION['descripTipomuestra']; ?></td>	
					</tr>
				</table>
                
                <table align="center" border="2">
					<tr>
						<td id="vaso1"></td>
					</tr>
					<tr>
						<td></td>
					</tr>
					<tr>
						<td></td>
					</tr>
				</table>
                
                <table align="center" border="2">
					<tr>
						<td>MUESTRA # 2</td>
						<td><?php echo $_SESSION['descripMuestra2']; ?></td>
					</tr>
				</table>
                
                <table align="center" border="2">
					<tr>
						<td></td>
					</tr>
					<tr>
						<td></td>
					</tr>
					<tr>
						<td></td>
					</tr>
				</table>
                
                <table align="center" border="2">
					<tr>
						<td>MUESTRA # 3</td>
						<td><?php echo $_SESSION['descripMuestra3']; ?></td>
					</tr>
				</table>
                
                <table align="center" border="2">
					<tr>
						<td></td>
					</tr>
					<tr>
						<td></td>
					</tr>
					<tr>
						<td></td>
					</tr>
				</table>
            </table>
  	</tr>
    <tr>
    	<td height="46"	 align="center">
        	<table align="center">
            	<tr>
                	<td><input type="submit" name="bt_buscamuestra" id="bt_buscamuestra" value="BUSCAR" onclick="click_btnmuestra()"/></td>
                    <td><input type="submit" name="btn_clistaecs" id="btn_listaecs" value="CANCELAR" /></td>
                </tr>
            </table>
        </td>
     </tr>               
  </table>
</form>

</body>
</html>
<script  language="javascript">
		
	
		$(document).ready(function()
		{
			$("#cbx_cargamuestra").change(function(){
				click_btnmuestra();
				console.log('entro');
				
				});
			
			/*$("#bt_buscamuestra").click(function()
			{
				id_carga_mst= $("#cbx_cargamuestra").val();
				alert('estoy aqui en el ajax'.id_carga_mst);
				console.log(id_carga_mst);
					
				$("#cbx_cargamuestra option:selected").each(function()
				{
					/*id_carga_mst= $(this).val();
					//alert("estoy en el combobox"+id_descgral);
					$.ajax({
						url: '../require/getInfovasos.php',
						type: 'POST',
						data: { id_descgral: id_descgral
							 },
						success: function(data) { 
							console.log(data);
							$("#cbx_vaso1").html(data);
						},
						error: function() {
							alert('<p>An error has occurred</p>');
						}
						});  		 
				});
			});*/
		});
	
	
		function click_btnmuestra()
		{
			var id_carga_mst= $("#cbx_cargamuestra").val();
			//console.log(id_carga_mst);
			$.ajax({
			url: '../require/getInfovasos.php',
			type: 'POST',
			data: { id_carga_mst: id_carga_mst
				 },
			success: function(data) { 
				var resulta=$.parseJSON(data);
				console.log(resulta);
				$.each(resulta, function(index, value){
					console.log(value.mstcv_codgral);
					}); 
			},
			error: function() {
				alert('<p>An error has occurred</p>');
			}
			});
		}


	
    </script>
