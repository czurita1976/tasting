<?php 
	require ("../controller/classDAO.php");
	$DAOsql=new DAOsql;
	$resultado=$DAOsql->findTipomuestras();
	//var_dump ($resultado);
?>							

						
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<div id="ccs-header" align="center"><?php require("../menu/menuMain.php");?></div>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>REGISTRO GENERAL DE MUESTRAS</title>
    <script language="javascript" src=../js/jquery-3.2.1.min.js></script>
    <LINK REL=StyleSheet HREF=" " TYPE="text/css" MEDIA=screen>
</head>
<body>
<form id="cargaMuestras" name="cargaMuestras" method="post" action="../controller/classMain.php">
  
  <table class="DashPartTitle2" width="60%" style="margin-left:24%;">
  	<tr class="">
    	<td align="center" ></td>
   	</tr>
  	<tr>
  		<td align="center" style="height:35px;"></td>
  	</tr>
    <tr>
  		<td align="center" style="height:35px;"><h4>Pantalla para el registro general de muestras (P-4)</h4></td>
  	</tr>
  	<tr>
  		<td height="46" align="center">
        	<table align="center" border="1" class="table">
				<tr>
                	<td>DESCRIPCION GENERAL DE LA MUESTRA</td>
                    <td><input type="text" name="tx_descgral" id="tx_descgral" style="width:300px" class="form-control"/></td>
                </tr>    
                <tr>
                	<td>TIPO DE MUESTRA</td>
                	<td>
						<select id="cbx_tipomuestra" name ="cbx_tipomuestra" class="form-control">
							<option value="0">Seleccione el Tipo de Muestra</option>
							<?php 
								$contar=0;
								foreach ($resultado as $row){ 
								if ($contar!=count($row)-1)
								{?>
								<option value="<?php echo $row['idevalscm_ttm'];?>"><?php echo $row['descecscm_ttm'];?></option>
							<?php }} 
								unset($row);
							?>
						</select>	
					</td>
                </tr>
                
                <table align="center" border="2" class="table">
					<tr>
						<td>DESCRIPCION MUESTRA # 1</td>
						<td><input type="text" name="tx_desmst_1" id="txt_desmst_1" style="width:540px" class="form-control DM1" placeholder="Descripcion Muestra"/></td>
					</tr>
				</table>
                
                <table align="center" border="2" class="table">
					<tr>
						<td><input type="number" name="tx_m1_v1" min="1" max="999" id="tx_m1_v1" style="width:150px" class="form-control M1"/></td>
						<td><input type="number" name="tx_m1_v2" min="1" max="999" id="tx_m1_v2" style="width:150px" class="form-control M1"/></td>
						<td><input type="number" name="tx_m1_v3" min="1" max="999" id="tx_m1_v3" style="width:150px" class="form-control M1"/></td>
						<td><input type="number" name="tx_m1_v4" min="1" max="999" id="tx_m1_v4" style="width:150px" class="form-control M1"/></td>
					</tr>
					<tr>
						<td><input type="number" name="tx_m1_v5" min="1" max="999" id="tx_m1_v5" style="width:150px" class="form-control M1"/></td>
						<td><input type="number" name="tx_m1_v6" min="1" max="999" id="tx_m1_v6" style="width:150px" class="form-control M1"/></td>
						<td><input type="number" name="tx_m1_v7" min="1" max="999" id="tx_m1_v7" style="width:150px" class="form-control M1"/></td>
						<td><input type="number" name="tx_m1_v8" min="1" max="999" id="tx_m1_v8" style="width:150px" class="form-control M1"/></td>
					</tr>
					<tr>
						<td><input type="number" name="tx_m1_v9" min="1" max="999" id="tx_m1_v9" style="width:150px" class="form-control M1"/></td>
						<td><input type="number" name="tx_m1_v10" min="1" max="999" id="tx_m1_v10" style="width:150px" class="form-control M1"/></td>
						<td><input type="number" name="tx_m1_v11" min="1" max="999" id="tx_m1_v11" style="width:150px" class="form-control M1"/></td>
						<td><input type="number" name="tx_m1_v12" min="1" max="999" id="tx_m1_v12" style="width:150px" class="form-control M1"/></td>
					</tr>
				</table>
                
                <table align="center" border="2" class="table">
					<tr>
						<td>DESCRIPCION MUESTRA # 2</td>
						<td><input type="text" name="tx_desmst_2" id="tx_desmst_2" style="width:540px"/></td>
					</tr>
				</table>
                
                <table align="center" border="2" class="table">
					<tr>
						<td><input type="number" name="tx_m2_v1" min="1" max="999" id="tx_m2_v1" style="width:150px" class="form-control"/></td>
						<td><input type="number" name="tx_m2_v2" min="1" max="999" id="tx_m2_v2" style="width:150px" class="form-control"/></td>
						<td><input type="number" name="tx_m2_v3" min="1" max="999" id="tx_m2_v3" style="width:150px" class="form-control"/></td>
						<td><input type="number" name="tx_m2_v4" min="1" max="999" id="tx_m2_v4" style="width:150px" class="form-control"/></td>
					</tr>
					<tr>
						<td><input type="number" name="tx_m2_v5" min="1" max="999" id="tx_m2_v5" style="width:150px" class="form-control"/></td>
						<td><input type="number" name="tx_m2_v6" min="1" max="999" id="tx_m2_v6" style="width:150px" class="form-control"/></td>
						<td><input type="number" name="tx_m2_v7" min="1" max="999" id="tx_m2_v7" style="width:150px" class="form-control"/></td>
						<td><input type="number" name="tx_m2_v8" min="1" max="999" id="tx_m2_v8" style="width:150px" class="form-control"/></td>
					</tr>
					<tr>
						<td><input type="number" name="tx_m2_v9" min="1" max="999" id="tx_m2_v9" style="width:150px" class="form-control"/></td>
						<td><input type="number" name="tx_m2_v10" min="1" max="999" id="tx_m2_v10" style="width:150px" class="form-control"/></td>
						<td><input type="number" name="tx_m2_v11" min="1" max="999" id="tx_m2_v11" style="width:150px" class="form-control"/></td>
						<td><input type="number" name="tx_m2_v12" min="1" max="999" id="tx_m2_v12" style="width:150px" class="form-control"/></td>
					</tr>
				</table>
                
                <table align="center" border="2" class="table">
					<tr>
						<td>DESCRIPCION MUESTRA # 3</td>
						<td><input type="text" name="tx_desmst_3" id="tx_desmst_3" style="width:540px"/></td>
					</tr>
				</table>
                
                <table align="center" border="2" class="table">
					<tr>
						<td><input type="number" name="tx_m3_v1" min="1" max="999" id="tx_m3_v1" style="width:150px" class="form-control"/></td>
						<td><input type="number" name="tx_m3_v2" min="1" max="999" id="tx_m3_v2" style="width:150px" class="form-control"/></td>
						<td><input type="number" name="tx_m3_v3" min="1" max="999" id="tx_m3_v3" style="width:150px" class="form-control"/></td>
						<td><input type="number" name="tx_m3_v4" min="1" max="999" id="tx_m3_v4" style="width:150px" class="form-control"/></td>
					</tr>
					<tr>
						<td><input type="number" name="tx_m3_v5" min="1" max="999" id="tx_m3_v5" style="width:150px" class="form-control"/></td>
						<td><input type="number" name="tx_m3_v6" min="1" max="999" id="tx_m3_v6" style="width:150px" class="form-control"/></td>
						<td><input type="number" name="tx_m3_v7" min="1" max="999" id="tx_m3_v7" style="width:150px" class="form-control"/></td>
						<td><input type="number" name="tx_m3_v8" min="1" max="999" id="tx_m3_v8" style="width:150px" class="form-control"/></td>
					</tr>
					<tr>
						<td><input type="number" name="tx_m3_v9" min="1" max="999" id="tx_m3_v9" style="width:150px" class="form-control"/></td>
						<td><input type="number" name="tx_m3_v10" min="1" max="999" id="tx_m3_v10" style="width:150px" class="form-control"/></td>
						<td><input type="number" name="tx_m3_v11" min="1" max="999" id="tx_m3_v11" style="width:150px" class="form-control"/></td>
						<td><input type="number" name="tx_m3_v12" min="1" max="999" id="tx_m3_v12" style="width:150px" class="form-control"/></td>
					</tr>
				</table>
			</table>
  	</tr>
    <tr>
    	<td height="46"	 align="center">
        	<table align="center" style="margin-bottom: 10%;">
            	<tr>
                	<td><input class='btn btn-flat' type="submit" name="bt_acregmuest" id="bt_acregmuest" value="ACEPTAR" /></td>
                    <td><input class='btn btn-flat' type="submit" name="bt_caregmuest" id="bt_caregmuest" value="CANCELAR" /></td>
                </tr>
            </table>
        </td>
     </tr>               
  </table>
</form>
