<?php 
	require ("../controller/classDAO.php");
	$DAOsql=new DAOsql;
	$resultado=$DAOsql->cbxUsuario();
?>		
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<div id="ccs-header" align="center"><?php require("../menu/menuMain.php");?></div>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>TASTE POLAR</title>
    <link rel='stylesheet' href=' '>
</head>
<body>
<form id="resetPwd" name="resetPwd" method="post" action="../controller/classMain.php">
  
  <table class="DashPartTitle4" width="100%">
  	<tr class="">
    	<td align="center" ></td>
   	</tr>
  	<tr>
  		<td align="center" style="height:35px;"></td>
  	</tr>
	<tr>
  		<td align="center" style="height:35px;"><h4>Pantalla para poder restablecer el password de los usuarios (P-20)</h4></td>
  	</tr>
  	<tr>
  		<td height="46" align="center">
        	<table align="center" border="1">
				<tr>
                	<td>USUARIO DE LA APP: </td>
                	<td>
						<select id="cbx_appUsuario" name ="cbx_appUsuario">
							<option value="0">Seleccione el Usuario de la APP</option>
							<?php 
								$contar=0;
								foreach ($resultado as $row){ 
								if ($contar!=count($row)-1)
								{?> 
								<option value="<?php echo $row['iduser_usrm'];?>"><?php echo $row['nuser_usrm'];?></option>
							<?php }} 
								unset($row);
							?>
						</select>
					</td>
                </tr>
                <tr>
					<td>CLAVE: </td>
					<td><input type="text" name="tx_clave" id="tx_clave" style="width:300px"/></td>
                </tr>    
				
				<tr>
                	<td>CONFIRME LA CLAVE: </td>
                    <td><input type="text" name="tx_confclave" id="tx_confclave" style="width:300px"/></td>
				</tr>
            </table>    
         </td>
  	</tr>
    <tr>
    	<td height="46"	 align="center">
        	<table align="center">
            	<tr>
                	<td><input type="submit" name="btna_rpuserfrm" id="btna_ruserfrm" value="ACEPTAR" /></td>
                    <td><input type="submit" name="btnc_rpuserfrm" id="btnc_ruserfrm" value="CANCELAR" /></td>
                </tr>
            </table>
        </td>
     </tr>               
  </table>
</form>
