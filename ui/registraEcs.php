<?php 
	require ("../controller/classDAO.php");
	
	$DAOsql=new DAOsql;
	$resultado=$DAOsql->cbxMuestrageneral();
	$resultado1=$DAOsql->cbxJuezsensorial();
	
	//echo json_encode($resultado);
	//echo json_encode($resultado1);
	
	//var_dump ($resultado);
	//var_dump ($resultado1);
?>							
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<div id="ccs-header" align="center"><?php require("../menu/menuMain.php");?></div>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>TASTE POLAR</title>
    <script language="javascript" src=../js/jquery-3.2.1.min.js></script>
    <script  language="javascript">
		
	

		$(document).ready(function()
		{
			$("#cbx_descgral").change(function()
			{
				$("#cbx_descgral option:selected").each(function()
				{
					id_descgral= $(this).val();
					//alert("estoy en el combobox"+id_descgral);
					
					 
					 $.ajax({
							url: '../require/getVasos.php',
							type: 'POST',
							data: { id_descgral: id_descgral
								 },
							success: function(data) { 
								console.log(data);
								$("#cbx_vaso1").html(data);
							},
							error: function() {
								alert('<p>An error has occurred</p>');
							}
							}); 
					
					$.ajax({
							url: '../require/getVasos1.php',
							type: 'POST',
							data: { id_descgral: id_descgral
								 },
							success: function(data) { 
								console.log(data);
								$("#cbx_vaso2").html(data);
							},
							error: function() {
								alert('<p>An error has occurred</p>');
							}
							});
					$.ajax({
							url: '../require/getVasos2.php',
							type: 'POST',
							data: { id_descgral: id_descgral
								 },
							success: function(data) { 
								console.log(data);
								$("#cbx_vaso3").html(data);
							},
							error: function() {
								alert('<p>An error has occurred</p>');
							}
							}); 		 
				});
			});
		});
		
    </script>
    <link rel='stylesheet' href=' '>
</head>
<body>
<form id="cargaEsc" name="cargaEsc" method="post" action="../controller/classMain.php">
  
  <table class="DashPartTitle3" width="100%">
  	<tr class="">
    	<td align="center" ></td>
   	</tr>
  	<tr>
  		<td align="center" style="height:35px;"></td>
  	</tr>
	<tr>
  		<td align="center" style="height:35px;"><h4>Pantalla para el registro de Evaluacion de Calidad Sensorial(ECS) (P-6)</h4></td>
  	</tr>
  	<tr>
  		<td height="46" align="center">
        	<table align="center" border="1">
				<tr>
                	<td>DESCRIPCION GENERAL DE LA MUESTRA</td>
                    <td>
						<select id="cbx_descgral" name ="cbx_descgral">
							<option value="0">Seleccione la Muestra General</option>
							<?php 
								$contar=0;
								foreach ($resultado as $row){ 
								if ($contar!=count($row)-1)
								{?>
								<option value="<?php echo $row['id_carga_mst'];?>"><?php echo $row['desc_carga_mst'];?></option>
							<?php }}
								unset($row);
							?>
						</select>
					</td>
				</tr>
            	<tr>
                	<td>JUEZ SENSORIAL</td>
                	<td>
						<select id="cbx_juezsenso" name ="cbx_juezsenso">
							<option value="0">Seleccione el Juez Sensorial</option>
							<?php 
								$contar1=0;
								foreach ($resultado1 as $row1){ 
								if ($contar1!=count($row1)-1)
								{?>
								<option value="<?php echo $row1['iduser_usrm'];?>"><?php echo $row1['pnuser_usrm'].' '.$row1['snuser_usrm'].' '.$row1['pauser_usrm'].' '.$row1['sauser_usrm'];?></option>
							<?php }} 
								unset($row1);
							?>
						</select>	
                	</td>
                </tr>    
               	<tr>
                	<td>EVALUACION DE CALIDAD SENSORIAL</td>
                    <td><input type="text" name="tx_regecs" id="tx_regecs" style="width:300px"/></td>
                </tr>    
  
            	<table align="center" border="2">
					<tr>
						<td>                                                                    </td>
					</tr>    
            	</table>
            	
                <table align="center" border="2">
					<tr>
						<td>MUESTRA # 1</td>
						<td>
							<select id="cbx_vaso1" name ="cbx_vaso1">
							</select>	
						</td>
						<td>MUESTRA # 2</td>
						<td>
							<select id="cbx_vaso2" name ="cbx_vaso2">
							</select>		
						</td>
						<td>MUESTRA # 3</td>
						<td>
							<select id="cbx_vaso3" name ="cbx_vaso3">
							</select>		
						</td>
					</tr>
				</table>
            </table>
  	</tr>
    <tr>
    	<td height="46"	 align="center">
        	<table align="center">
            	<tr>
                	<td><input type="submit" name="btn_aregecs" id="btn_aregecs" value="ACEPTAR" /></td>&nbsp; &nbsp;
                    <td><input type="submit" name="btn_cregecs" id="btn_cregecs" value="CANCELAR" /></td>
                </tr>
            </table>
        </td>
     </tr>               
  </table>
 </form>
</body>
</html>
