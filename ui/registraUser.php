<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<div id="ccs-header" align="center"><?php require("../menu/menuMain.php");?></div>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>TASTE POLAR</title>
    <link rel='stylesheet' href=' '>
</head>
<body>
<form id="registraUser" name="registraUser" method="post" action="../controller/classMain.php">
  
  <table class="DashPartTitle4" width="100%">
  	<tr class="">
    	<td align="center" ></td>
   	</tr>
  	<tr>
  		<td align="center" style="height:35px;"></td>
  	</tr>
	<tr>
  		<td align="center" style="height:35px;"><h4>Pantalla para el registro de usuarios en la aplicacion (P-13)</h4></td>
  	</tr>
  	<tr>
  		<td height="46" align="center">
        	<table align="center" border="1">
				<tr>
                	<td>NOMBRE DE USUARIO: </td>
                	<td><input type="text" name="tx_username" id="tx_username" style="width:300px"/></td>
                </tr>
            	<tr>
                	<td>PASSWORD</td>
                	<td><input type="text" name="tx_pwd" id="tx_pwd" style="width:300px"/></td>
				</tr>
                <tr>
                	<td>CONFIRME EL PASSWORD</td>
                	<td><input type="text" name="tx_pwdc" id="tx_pwdc" style="width:300px"/></td>
                </tr>
                <tr>
                	<td>PRIMER NOMBRE: </td>
                    <td><input type="text" name="tx_pnuser" id="tx_pnuser" style="width:300px"/></td>
                </tr>
                <tr>
                	<td>SEGUNDO NOMBRE: </td>
                    <td><input type="text" name="tx_snuser" id="tx_snuser" style="width:300px"/></td>
                </tr>
                <tr>
                	<td>PRIMER APELLIDO: </td>
                    <td><input type="text" name="tx_pauser" id="tx_pauser" style="width:300px"/></td>
                </tr>
                <tr>
                	<td>SEGUNDO APELLIDO: </td>
                    <td><input type="text" name="tx_sauser" id="tx_sauser" style="width:300px"/></td>
                </tr>    
                <tr>
                	<td>CEDULA DE IDENTIDAD: </td>
                    <td><input type="text" name="tx_ciuser" id="tx_ciuser" style="width:300px"/></td>
                </tr>
                <tr>
                	<td>CARGO: </td>
                    <td>
        				<select id="cbx_tipoUsuario" name="cbx_tipoUsuario">
							<option value="0">Seleccione el Tipo de Usuario</option>
							<option value="1">ANALISTA</option>
							<option value="2">jUEZ SENSORIA</option>
							<option value="3">ADMINISTRADOR</option>
						</select>
					</td>
                </tr>
            	<tr>
                	<td>DIRECCION PRINCIPAL: </td>
                    <td><input type="text" name="tx_dir1user" id="tx_dir1user" style="width:300px"/></td>
                </tr>
                <tr>
                	<td>DIRECCION SECUNDARIA: </td>
                    <td><input type="text" name="tx_dir2user" id="tx_dir2user" style="width:300px"/></td>
                </tr>
                <tr>
                	<td>TELEFONO # 1: </td>
                    <td><input type="text" name="tx_tlf1user" id="tx_tlf1user" style="width:300px"/></td>
                </tr>
                <tr>
                	<td>TELEFONO # 2: </td>
                    <td><input type="text" name="tx_tlf2user" id="tx_tlf2user" style="width:300px"/></td>
                </tr>
                <tr>
                	<td>TELEFONO # 3: </td>
                    <td><input type="text" name="tx_tlf3user" id="tx_tlf3user" style="width:300px"/></td>
                </tr>
            	<tr>
                	<td>CORREO ELECTRONICO: </td>
                    <td><input type="text" name="tx_emailuser" id="tx_emailuser" style="width:300px"/></td>
                </tr>
           </table>
  	</tr>
    <tr>
    	<td height="46"	 align="center">
        	<table align="center">
            	<tr>
                	<td><input type="submit" name="btna_ruserfrm" id="btna_ruserfrm" value="ACEPTAR" /></td>
                    <td><input type="submit" name="btnc_ruserfrm" id="btnc_ruserfrm" value="CANCELAR" /></td>
                </tr>
            </table>
        </td>
     </tr>               
  </table>
</form>
