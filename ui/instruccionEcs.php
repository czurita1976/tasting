<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<div id="ccs-header" align="center"><?php require("../menu/menuMain.php");?></div>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>TASTE POLAR</title>
    <link rel='stylesheet' href=' '>
</head>
<body>
<form id="instruccionEsc" name="instruccionEsc" method="post" action="../controller/classMain.php">
  
  <table class="DashPartTitle3" width="100%">
  	<tr class="">
    	<td align="center" ></td>
   	</tr>
  	<tr>
  		<td align="center" style="height:35px;"></td>
  	</tr>
	<tr>
  		<td align="center" style="height:35px;"><h4>Pantalla para la lectura de las instrucciones de las ECS (P-11)</h4></td>
  	</tr>
  	<tr>
  		<td height="46" align="center">
        	<table align="center" border="1">
				<tr>
                	<td><h4>FECHA DEL SISTEMA</h4></td>
					<td>
					<?php
						date_default_timezone_set("America/Caracas");
						echo date ("Y/m/d");
					?>
                    </td>
                </tr>
                
                <tr>
                	<td><h4>HORA DEL SISTEMA</h4></td>
                    <td>
					<?php
						date_default_timezone_set("America/Caracas");
						echo date ("h:i:sa");
					?>
                    </td>
                </tr>
                <tr>
                	<td><h4>DESCRIPICION EVALUACION DE CALIDAD SENSORIAL</h4></td>
                	<td>
						<?php 
                          session_start();
                          $_SESSION['codEcs'];
                          echo $_SESSION['descripTipomuestra'];
						?>
						<input type="hidden" name="codEcs" value="<?php echo $_SESSION['codEcs'];?>">
                	</td>
                </tr>
				<tr>
					<td><h4>TIPO DE MUESTRA</h4></td>
                	<td>
						<?php 
                          echo $_SESSION['tipoMuestra1'];
						?>
                	</td>
                </tr>    
            	<table align="center" border="2">
					<tr>
						<td>                                                                    </td>
					</tr>    
            	</table>
            	
                <table align="center" border="2">
					<tr>
						<td align="center"><h2>INSTRUCCIONES</h2></td>
					</tr>
					<tr>
						<td><p><?php echo $_SESSION['varInstrucciones']; ?></p></td>
					</tr>
				</table>
            </table>
  	</tr>
    <tr>
    	<td height="46"	 align="center">
        	<table align="center">
            	<tr>
                	<td><input type="submit" name="btn_ainstrud" id="btn_ainstrud" value="ACEPTAR" /></td>&nbsp; &nbsp;
                    <td><input type="submit" name="btn_cinstrud" id="btn_cinstrud" value="CANCELAR" /></td>
                </tr>
            </table>
        </td>
     </tr>               
  </table>
</form>
