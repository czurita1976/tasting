<?php 
	require ("../controller/classDAO.php");
	$DAOsql=new DAOsql;
	$resultado=$DAOsql->findTipomuestras();
?>			
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<div id="ccs-header" align="center"><?php require("../menu/menuMain.php");?></div>
<head>
   <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>TASTE POLAR</title>
    <script language="javascript" src=../js/jquery-3.2.1.min.js></script>
    <script  language="javascript">
		
	
	function click_buscaDatosmst()
	{
		var id_select= $("#cbx_tipomst").val();
		
		// se listan Todas las ECS
		if (id_select==1)
		{
			alert(id_select); // debe ir 
			$.ajax({
				url: '../require/getBuscainfomst.php',
				type: 'POST',
				data: { id_select: id_select
					 },
				success: function(data) { 
					console.log(data);
					//$("#cbx_vaso1").html(data);
				},
				error: function() {
					alert('<p>An error has occurred</p>');
				}
			});  		 
		}
		
		// se listan ECS Pendientes
		if (id_select==2)
		{
			alert(id_select);
		}
		
		// se listan ECS Efectuadas
		if (id_select==3)
		{
			alert(id_select);
		}
	}



		/*$(document).ready(function()
		{
			$("#bt_buscamuestra").click(function()
			{
				id_carga_mst= $("#cbx_cargamuestra").val();
				alert('estoy aqui en el ajax'.id_carga_mst);
				console.log(id_carga_mst);
					
				$("#cbx_cargamuestra option:selected").each(function()
				{
					/*id_carga_mst= $(this).val();
					//alert("estoy en el combobox"+id_descgral);
					$.ajax({
						url: '../require/getInfovasos.php',
						type: 'POST',
						data: { id_descgral: id_descgral
							 },
						success: function(data) { 
							console.log(data);
							$("#cbx_vaso1").html(data);
						},
						error: function() {
							alert('<p>An error has occurred</p>');
						}
						});  		 
				});
			});
		});*/
		
    </script>
    <link rel='stylesheet' href=' '>
</head>
<body>
<form id="adMuestra" name="adMuestra" method="post" action="classContructor1.php">
  
  <table class="DashPartTitle4" width="100%">
  	<tr class="">
    	<td align="center" ></td>
   	</tr>
  	<tr>
  		<td align="center" style="height:35px;"></td>
  	</tr>
	<tr>
  		<td align="center" style="height:35px;"><h4>Pantalla para la activacion y desactivacion del los tipos de muestra en la aplicacion (P-18)</h4></td>
  	</tr>	
  	<tr>
  		<td height="46" align="center">
        	<table align="center" border="1">
				<tr>
                	<td>DESCRIPCION DEL TIPO MUESTRA</td>
                	<td>
						<select id="cbx_tipomst" name ="cbx_tipomst">
							<option value="0">Seleccione el Tipo de Muestra</option>
							<?php 
								$contar=0;
								foreach ($resultado as $row){ 
								if ($contar!=count($row)-1)
								{?>
								<option value="<?php echo $row['idevalscm_ttm'];?>"><?php echo $row['descecscm_ttm'];?></option>
							<?php }}
								unset($row);
							?>
						</select>
					</td>
                </tr>
                <tr>
					<td>INSTRUCCIONES: </td>
                	<td>
						<?php
							echo 'INSTRUCCIONE PARA REALIZAR LA EVALUACION DE CALIDAD SENSORIAL ';
						?>
                	</td>
                </tr>
            	<tr>
                	<td>ESTATUS: </td>
                    <td align="center">
        				<select id="cbx_statusMst" name ="cbx_statusMst">
							<option value="0">Selecccion el estatus</option>
							<option value="1">ACTIVAR</option>
							<option value="2">DESACTIVAR</option>
						</select>
					</td>
				</tr>
            </table>    
         </td>
  	</tr>
    <tr>
    	<td height="46"	 align="center">
        	<table align="center">
            	<tr>
                	<td><input type="submit" name="btna_aodmst" id="btna_aodmst" value="ACEPTAR" /></td>
                    <td><input type="submit" name="btnc_aodmst" id="btnc_aodmst" value="CANCELAR" /></td>
                </tr>
            </table>
        </td>
     </tr>               
  </table>
</form>
