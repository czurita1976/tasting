<?php 
	require ("../controller/classDAO.php");
	$DAOsql=new DAOsql;
	$resultado=$DAOsql->cbxUsuario();
?>		
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>TASTE POLAR</title>
    <link rel='stylesheet' href=' '>
</head>
<body>
<form id="aodUser" name="aodUser" method="post" action="../controller/classMain.php">
  
  <table class="DashPartTitle4" width="100%">
  	<tr class="">
    	<td align="center" ></td>
   	</tr>
  	<tr>
  		<td align="center" style="height:35px;"></td>
  	</tr>
	<tr>
  		<td align="center" style="height:35px;"><h4>Pantalla para la activacion y desactivacion de usuarios en la aplicacion (P-15)</h4></td>
  	</tr>
  	<tr>
  		<td height="46" align="center">
        	<table align="center" border="1">
				<tr>
                	<td>USUARIO DE LA APP: </td>
                	<td>
						<select id="cbx_appUsuario" name ="cbx_appUsuario">
							<option value="0">Seleccione el Usuario de la APP</option>
							<?php 
								$contar=0;
								foreach ($resultado as $row){ 
								if ($contar!=count($row)-1)
								{?> 
								<option value="<?php echo $row['iduser_usrm'];?>"><?php echo $row['nuser_usrm']; 
								if ($row['status_usrm']==1) {$statusUser= 'ACTIVO';}
								else if ($row['status_usrm']==2) {$statusUser= 'DESACTIVO';}
								;?></option>
							<?php }} 
								unset($row);
							?>
						</select>
					</td>
                </tr>
                <tr>
					<td>
						ESTATUS ACTUAL
					</td>
					<td>
						<?php echo $statusUser;?>
					</td>
				</tr>	
            	<tr>
                	<td>ESTATUS: </td>
                    <td align="center">
        				<select id="cbx_statusUsuario" name ="cbx_statusUsuario">
							<option value="0">Selecccion el estatus</option>
							<option value="1">ACTIVAR</option>
							<option value="2">DESACTIVAR</option>
						</select>
					</td>
				</tr>
            </table>    
         </td>
  	</tr>
    <tr>
    	<td height="46"	 align="center">
        	<table align="center">
            	<tr>
                	<td><input type="submit" name="btna_aoduserfrm" id="btna_ruserfrm" value="ACEPTAR" /></td>
                    <td><input type="submit" name="btnc_aoduserfrm" id="btnc_ruserfrm" value="CANCELAR" /></td>
                </tr>
            </table>
        </td>
     </tr>               
  </table>
</form>
