function nuevoAjax()
{
	var xmlhttp=false;
 	try 
	{
		xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
	} catch (e) {
 		try {
 			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
 		} catch (E) {
 			xmlhttp = false;
 		}
  	}

	if (!xmlhttp && typeof XMLHttpRequest!='undefined') 
	{
 		xmlhttp = new XMLHttpRequest();
	}
	return xmlhttp;
}

//AJAX TIMEOUT
function ajaxTimeoutFunction(div_id)
{
	//mato el ajax
	ajax.abort();
	
	//limpio el div (si as� se pide)
	if(typeof div_id!="undefined")
		document.getElementById(div_id).innerHTML = "";
	
	//despliego mensaje
	alert("Disculpe!!!\nParece que la conexion es muy lenta o el servidor tiene muchas solicitudes...\nPor favor, intente de nuevo");
}